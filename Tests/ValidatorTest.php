<?php

require_once("PHPUnit/Framework.php");
require_once("Formlib/Validators/Validator.php");

class ValidatorTest extends PHPUnit_Framework_TestCase
{
	protected $_validator = null;
	protected $_testRegex = "/^[A-Za-z0-9]+$/";

	public function setup()
	{
		$this->_validator = new Validator($this->_testRegex);
	}

	public function tearDown()
	{
		unset($this->_validator);
	}

	public function testValidData()
	{
		$this->assertTrue($this->_validator->validate("Hello"));
	}
	
	public function testInvalidData()
	{
		$this->assertFalse($this->_validator->validate("Hello!"));
	}
}

?>
