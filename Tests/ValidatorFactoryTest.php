<?php

require_once("PHPUnit/Framework.php");
require_once("Formlib/Validators/ValidatorFactory.php");

class ValidatorFactoryTest extends PHPUnit_Framework_TestCase
{
	protected $_validatorFactory = null;

	public function setup()
	{
		$this->_validatorFactory = new ValidatorFactory();
	}

	public function tearDown()
	{
		unset($this->_validatorFactory);
	}

	public function testAvailableValidator()
	{
		$this->_validatorFactory->addValidator("test", "/^[A-Za-z]+$/");
		$validator = $this->_validatorFactory->getValidator("test");

		$this->assertTrue(is_a($validator, "Validator"));
	}

	/**
	 * @expectedException MissingValidatorException
	 */
	public function testUnavailableValidator()
	{
		$validator = $this->_validatorFactory->getValidator("doesntExist");
	}
}

?>
