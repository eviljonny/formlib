<?php

require_once("PHPUnit/Framework.php");
require_once("Formlib/Config/MetaFileParser.php");

class MetaFileParserExpose extends MetaFileParser
{
	public function _isValidColumnName($column)
	{
		return parent::_isValidColumnName($column);
	}

	public function _loadMetaFile()
	{
		return parent::_loadMetaFile();
	}

	public function getFileHandle()
	{
		return $this->_file_handle;
	}

	public function setPage($page)
	{
		$this->_page = $page;
	}
}

class MetaFileParserTest extends PHPUnit_Framework_TestCase
{
	protected $_basedir;
	protected $_read_only_file;
	protected $_confirmation_string;


	public function setup()
	{
		$this->_basedir = getcwd();
		$this->_read_only_file = $this->_basedir . "/metafiles/READ_ONLY_TEST.meta";
		$this->_confirmation_string = getmypid() . "-" . time() . "-" . rand();
	}

	public function tearDown()
	{
		if (file_exists($this->_read_only_file))
		{
			unlink($this->_read_only_file);

		}
	}

	/**
	 * @expectedException MetaFileException
	 */
	public function testNoPageSupplied()
	{
		new MetaFileParser($this->_basedir, null);
	}

	/**
	 * @expectedException MetaFileException
	 */
	public function testInvalidPageSupplied()
	{
		new MetaFileParser($this->_basedir, "HELLO!!!!");
	}

	public function testValidColumnName()
	{
		$parser = new MetaFileParserExpose($this->_basedir, "legal_name");
		$this->assertTrue($parser->_isValidColumnName("validColumnName"));
	}

	public function testInvalidColumnName()
	{
		$parser = new MetaFileParserExpose($this->_basedir, "legal_name");
		$this->assertFalse($parser->_isValidColumnName("columnName!"));
	}

	/**
	 * @expectedException MetaFileException
	 */
	public function testNonExistingMetafile()
	{
		$parser = new MetaFileParserExpose($this->_basedir, "legal_name");
		$parser->setPage("I_DONT_EXIST");

		$parser->_loadMetaFile();
	}

	public function testWorkingMetafile()
	{
		$fh = fopen($this->_read_only_file, "w+");
		if (!$fh)
		{
			throw new Exception("Couldn't open read only file " . $this->_read_only_file);
		}

		if (!fwrite($fh,$this->_confirmation_string))
		{
			throw Exception("Couldn't open read only file " . $this->_read_only_file);
		}

		fclose($fh);

		$parser = new MetaFileParserExpose($this->_basedir, "legal_name");
		$parser->setPage("READ_ONLY_TEST");

		$parser->_loadMetaFile();
	}

}

?>
