From nginx:1.11

MAINTAINER Jonathan Harden "jfharden@gmail.com"

RUN apt-get update && \
    apt-get install -y nginx-module-perl && \
    rm -rf /var/lib/apt/lists/* && \
    rm /etc/nginx/conf.d/default.conf && \
    rm /usr/share/nginx/html/index.html

COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx_conf.d/formlib-demo.conf /etc/nginx/conf.d/formlib-demo.conf

COPY example_project/ /usr/share/nginx/html/formlib-demo
COPY src/ /formlib-src

VOLUME /usr/share/nginx/html/formlib-demo
VOLUME /formlib-src

CMD nginx -g 'daemon off;'
