.PHONY: initdb

initdb:
	docker-compose up -d postgres-dev
	sleep 10 # Need to give postgres inside the container a few seconds to come online
	docker-compose exec postgres-dev /database_seeds/seed.sh
	docker-compose stop postgres-dev

