<?php
class FormlibConfig
{
	public static $database = array(
		'database' => "formlibdemo",
		'hostname' => "127.0.0.1",
		'username' => "formlibdemo",
		'password' => "shi3Ogho",
	);

	public static $formatting = array(
		'preform' => "<div class='form'>",
		'prerow' => "<div class='row'>",
		'prelabel' => "<div class='label'>",
		'postlabel' => "</div>",
		'prefield' => "<div class='field'>",
		'postfield' => "</div>",
		'preerror' => "<div class='error'>",
		'posterror' => "</div>",
		'postrow' => "</div>",
		'postform' => "</div>",
	);
}
?>
