<?php
  $time_start = microtime(true);
  print "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\" ?>";
?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<script src="formlib-assets/formlib.js" type="text/javascript"></script>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<title>
  Formlib Demo
</title>
<link href="formlib-assets/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="banner">
<h1 class="title">Formlib Demo</h1>
</div>
<div class="menu">
<ul>
  <li>
    <a href="index.php?page=items&amp;mode=search">Search Items</a>
  </li>
  <li>
    <a href="index.php?page=lists&amp;mode=search">Search Lists</a>
  </li>
  <li>
    <a href="index.php?page=types&amp;mode=search">Search Types</a>
  </li>
  <li>
    Add
    <ul>
      <li>
        <a href="index.php?page=items&amp;mode=new">Items</a>
      </li>
      <li>
        <a href="index.php?page=lists_dd&amp;mode=new">Lists Drop</a>
      </li>
      <li>
        <a href="index.php?page=lists_ddl&amp;mode=new">Lists Drop Lookup</a>
      </li>
      <li>
        <a href="index.php?page=lists_l&amp;mode=new">Lists List</a>
      </li>
      <li>
        <a href="index.php?page=lists_ll&amp;mode=new">Lists List Lookup</a>
      </li>
      <li>
        <a href="index.php?page=lists_lm&amp;mode=new">Lists Multi</a>
      </li>
      <li>
        <a href="index.php?page=lists_lml&amp;mode=new">Lists Multi Lookup</a>
      </li>
      <li>
        <a href="index.php?page=lists_r&amp;mode=new">Lists Radio</a>
      </li>
      <li>
        <a href="index.php?page=lists_rl&amp;mode=new">Lists Radio Lookup</a>
      </li>
      <li>
        <a href="index.php?page=types&amp;mode=new">Types</a>
      </li>
      <li>
        <a href="index.php?page=tarotcast&amp;mode=new">Tarotcast</a>
      </li>
    </ul>
  </li>
  <li>
    List
    <ul>
      <li>
        <a href="index.php?page=items&amp;mode=list">Items</a>
      </li>
      <li>
        <a href="index.php?page=lists_dd&amp;mode=list">Lists Drop</a>
      </li>
      <li>
        <a href="index.php?page=lists_ddl&amp;mode=list">Lists Drop Lookup</a>
      </li>
      <li>
        <a href="index.php?page=lists_l&amp;mode=list">Lists List</a>
      </li>
      <li>
        <a href="index.php?page=lists_ll&amp;mode=list">Lists List Lookup</a>
      </li>
      <li>
        <a href="index.php?page=lists_lm&amp;mode=list">Lists Multi</a>
      </li>
      <li>
        <a href="index.php?page=lists_lml&amp;mode=list">Lists Multi Lookup</a>
      </li>
      <li>
        <a href="index.php?page=lists_r&amp;mode=list">Lists Radio</a>
      </li>
      <li>
        <a href="index.php?page=lists_rl&amp;mode=list">Lists Radio Lookup</a>
      </li>
      <li>
        <a href="index.php?page=types&amp;mode=list">Types</a>
      </li>
      <li>
        <a href="index.php?page=tarotcast&amp;mode=list">Tarotcast</a>
      </li>
    </ul>
  </li><br />
</ul>
</div>
<div class="content">
<?php
  require('/formlib-src/formlib.php');

  generateForm("list", "lists_lml", dirname(__FILE__) . "/formlib_config");
?>
<?php
  $time_end = microtime(true);
  print "<br /><div style='text-align:center;font-size:smaller;margin-top:1em;'>Page took " . ($time_end - $time_start) . " seconds to generate</div>";
?>
</div>
</body>
</html>
