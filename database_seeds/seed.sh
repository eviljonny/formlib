#!/bin/sh

cd /database_seeds

echo "======================================================"
echo "Creating roles and databases"
echo "======================================================"
psql --host 127.0.0.1 -U postgres postgres < 01-roles_and_databases.psql
echo
echo
echo

echo "======================================================"
echo "Creating schema"
echo "======================================================"
psql --host 127.0.0.1 -U formlibdemo formlibdemo < 02-schema.psql
echo
echo
echo

echo "======================================================"
echo "Importing Data"
echo "======================================================"
psql --host 127.0.0.1 -U formlibdemo formlibdemo < 03-data.psql
echo
echo
echo
