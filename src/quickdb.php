<?php
$_quickdb = array(
    'pdo'    => null,
    'stmt'   => false,
    'driver' => null,
);

// Just a library for quickly interacting with a database
function connectToDatabase($dbname, $host, $user, $pass, $driver="pgsql") {
    global $_quickdb;

    $_quickdb['driver'] = $driver;

    try {
        $_quickdb['pdo'] = new PDO("$driver:host=$host;dbname=$dbname", $user, $pass);
    } catch (PDOException $e) {
        print "Couldn't connect to database!!! $e";
        return false;
    }

    return true;
}

function disconnectFromDatabase() {
    global $_quickdb;

    if ($_quickdb['stmt'] !== false) {
        $_quickdb['stmt']->closeCursor();
        $_quickdb['stmt'] = false;
    }
    $_quickdb['pdo'] = null;
}

function runReturnlessQuery($query, $params=array()) {
    global $_quickdb;

    $stmt = $_quickdb['pdo']->prepare($query);

    if ($stmt === false) {
        print "Failed to prepare query $query";
        return false;
    }

    if ($stmt->execute($params) === false) {
        print "Failed to execute query $query with params: ";
        var_dump($params);
        print "DB Error was: ";
        var_dump($stmt->errorInfo());
        return false;
    }

    return true;
}

function runInsertReturningID($query, $params=array(), $pk_column='id') {
    global $_quickdb;

    if ($_quickdb['driver'] === 'pgsql') {
        $query .= " RETURNING $pk_column";
    }

    $stmt = $_quickdb['pdo']->prepare($query);

    if ($stmt === false) {
        print "Failed to prepare query $query";
        return false;
    }

    if ($stmt->execute($params) === false) {
        print "Failed to execute query $query with params: ";
        var_dump($params);
        print "DB Error was: ";
        var_dump($stmt->errorInfo());
        return false;
    }

    if ($_quickdb['driver'] === 'pgsql') {
        $row = $stmt->fetch(PDO::FETCH_NUM);

        $stmt->closeCursor();
        $stmt = false;

        return $row[0];
    }
    elseif ($_quickdb['driver'] === 'mysql') {
        return $_quickdb['pdo']->lastInsertId();
    }
    else {
        print "Unknown driver, only mysql and pgsql supported for this function";
        return false;
    }
}

function runQueryReturnOne($query, $params = array()) {
    global $_quickdb;

    $row = null;

    $stmt = $_quickdb['pdo']->prepare($query);
    if ($stmt === false) {
        cleanUp();
        die("Couldn't prepare statement $query");
    }

    if ($stmt->execute($params) === false) {
        cleanUp();
        die("Couldn't execute the query $query");
    }

    if ($stmt->rowCount() != 1) {
        $stmt->closeCursor();
        return false;
    }

    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $stmt->closeCursor();

    return $row;
}

function getRecord($table,$pk_field,$lookup_key) {
    return runQueryReturnOne(
        'SELECT * FROM ' . $table . ' WHERE ' . $pk_field .'=?',
        array($lookup_key)
    );
}

function startMultiRowLookup($query, $params=array()) {
    global $_quickdb;

    $_quickdb['stmt'] = $_quickdb['pdo']->prepare($query);

    if ($_quickdb['stmt'] === false) {
        return false;
    }

    if ($_quickdb['stmt']->execute($params) === false) {
        return false;
    }

    return true;
}

function  startLookupReturnHandle($query, $params=array()) {
    global $_quickdb;

    $stmt = $_quickdb['pdo']->prepare($query);

    if ($stmt === false) {
        return false;
    }

    if ($stmt->execute($params) === false) {
        return false;
    }

    return $stmt;
}

function _getRow($type) {
    global $_quickdb;

    $row = $_quickdb['stmt']->fetch($type);

    if ($row === false) {
        $_quickdb['stmt']->closeCursor();
        $_quickdb['stmt'] = false;
    }

    return $row;
}

function getNextRowAssoc() {
    return _getRow(PDO::FETCH_ASSOC);
}

function getNextRow() {
    return _getRow(PDO::FETCH_NUM);
}

function numRows() {
    global $_quickdb;

    return $_quickdb['stmt']->rowCount();
}

function getLastError() {
    global $_quickdb;

    if ($_quickdb['stmt'] === false) {
        return $_quickdb['pdo']->errorInfo();
    }
    else {
        return $_quickdb['stmt']->errorInfo();
    }
}

?>
