<?php

# Update the include path to find the Formlib classes
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . "/");

/************************************************************************
* Global Variables                                                      *
************************************************************************/
$validation_types = Array();
$_FORMLIB_CONF = Array();
$metaInfo = array();
$lookups = array();
$list = array();
$list_order = "";
$FORM_TYPES = array('hidden','text','dropdown','dropdown_lookup','list',
            'list_lookup','list_multiselect','list_multiselect_lookup',
            'checkbox','radio','radio_lookup','textarea');
$modes = array("new"=>1,"edit"=>2,"insert"=>3,"update"=>4,"list"=>5,"search"=>6,"delete"=>7);
$mode = $modes['new'];
$lookup_key = null;
$file = "";
$table = "";
$pk_field = "";
$multikey = false;
$view = false;
$columnTypes = array();
# Note that these column types are not a representation of what the type is
# but of how we need to quote it (or not) to insert it
$postgresColumnTypes = array(
    "char" => "string",
    "character" => "string",
    "character varying" => "string",
    "varchar" => "string",
    "integer" => "numeric",
    "int" => "numeric",
    "int4" => "numeric",
    "numeric" => "numeric",
    "date" => "string",
    "text" => "string",
    "cidr" => "string",
    "bigint" => "numeric",
    "int8" => "numeric",
    "bigserial" => "numeric",
    "serial8" => "numeric",
    "boolean" => "boolean",
    "bool" => "boolean",
    "bytea" => "string",
    "double precision" => "numeric",
    "float8" => "numeric",
    "inet" => "string",
    "macaddr" => "string",
    "real" => "numeric",
    "float4" => "numeric",
    "int2" => "numeric",
    "smallint" => "numeric",
    "serial" => "numeric",
    "serial4" => "numeric",
    "time" => "string",
    "timestamp" => "string",
    "tsquery" => "string",
    "tsvector" => "string",
    "xml" => "string",
);

require('quickdb.php');

/************************************************************************
* Actual executed script for formlib                                    *
************************************************************************/
function generateForm($defaultMode = null, $defaultPage = null, $configPath = null)
{
    global $_FORMLIB_CONF,$_FORMLIB_CONFIG_PATH,$modes,$mode,$metaInfo;

    if (!isset($configPath) || $configPath === null || strlen(trim($configPath)) == 0)
    {
      print "<h1>Config Path not set correctly</h1>";
      die("configPath not valid");
    }

    $_FORMLIB_CONFIG_PATH = $configPath;

    formlib_init();

    connectToDatabase($_FORMLIB_CONF['db_name'], $_FORMLIB_CONF['db_host'], $_FORMLIB_CONF['db_user'], $_FORMLIB_CONF['db_pass'], $_FORMLIB_CONF['db_driver']);

    readMetaFile($defaultPage);
    getMode($defaultMode);

    printTitle();

    switch($mode)
    {
        case $modes['search']:
            populateLookups();
            break;
        case $modes['list']:
            populateLookups();
            break;
        case $modes['new']:
            populateLookups();
            break;
        case $modes['edit']:
            getLookupKey();
            populateVariables( );
            populateLookups();
            break;
        case $modes['delete']:
            if (confirmDelete())
            {
                deleteRows();
            }
            else
            {
                populateLookups();
            }
            break;
        case $modes['insert']:
            if (validateVars())
            {
                insertRecord();
                cleanUp();
                return;
            }
            else
            {
                $mode = $modes['new'];
                errorPassthrough();
            }

            break;
        case $modes['update']:
            if (validateVars())
            {
                updateRecord();
                cleanUp();
                return;
            }
            else
            {
                $mode = $modes['edit'];
                errorPassthrough();
            }
            break;
        default:
            cleanUp();
            die("ERROR: Could not understand what mode to be in");
            break;
    }

    // If we are in resolve dependencies we want to pass through whatever the fields were set to
    // when the dependency was resolved
    if (isset($_POST['resolve_dependency']))
    {
        foreach ($metaInfo as &$field)
        {
            $field['value'] = $_POST[$field['field']];
        }
    }

    if ($mode != $modes['list'] && $mode != $modes['delete'])
    {
        printFormElements();
    }
    else if ($mode == $modes['list'])
    {
        getList();
    }
    else if ($mode == $modes['delete'] && !confirmDelete())
    {
        getList(true);
    }

    cleanUp();
    /***********************************************************************/
}


/************************************************************************************************/
/* Functions follow                                                                             */
/************************************************************************************************/
function errorPassthrough()
{
    global $metaInfo;

    // We want to continue printing the form with our variables filled out and the
    // errors displayed so populate the lookup tables
    populateLookups();
    // Assign the vars passed back by the form as the vals in the form
    foreach ($metaInfo as &$field)
    {
        $field['value'] = $_POST[$field['field']];
    }
}

/************************************************************************
* This function outputs the form elements that are required from           *
* the meta file                                                            *
************************************************************************/
function printFormElements()
{
    global $metaInfo, $_FORMLIB_CONF, $mode, $modes;

    displayFormHeader();

    // For each field
    foreach($metaInfo as $field)
    {
        // If view only is set and the mode is new, update, insert or delete ignore this field and continue
        if (viewOnly($field) && ($mode == $modes['new'] || $mode == $modes['update'] || $mode == $modes['insert'] || $mode == $modes['delete']))
        {
            continue;
        }

        if (!hiddenField($field))
        {
            print $_FORMLIB_CONF['prerow'];
            displayLabel($field);
        }
        displayField($field);
        if (!hiddenField($field))
        {
            displayError($field);
            print $_FORMLIB_CONF['postrow'];
        }
    }
    // Finally the submit button
    print $_FORMLIB_CONF['prerow'];
    displayLabel(null);
    displayField(null);
    displayError(null);
    print $_FORMLIB_CONF['postrow'];

    // Print out the div footer
    displayFormFooter();
}

function deleteRows()
{
    global $metaInfo, $table, $pk_field;
    /*************************************
    Get the list of IDs to delete
    Delete all entries relating to this row in multikey tables
        Loop through the fields
            if field is multikey
                Delete from MK_TABLE where MAIN_PK_FIELD is one of our PKs
            end if
        end loop
    Delete all rows where the PK is one of the ones we have
    **************************************/
    if (!isset($_POST['_FORMLIB_delete']) || count($_POST['_FORMLIB_delete']) < 1)
    {
        cleanUp();
        die("Hey NAUGHTY. I will NOT do a global delete, you must specify IDs to delete");
    }
    $deleteList = $_POST['_FORMLIB_delete'];

    $where = "";

    // Now check to make sure they are all numeric values
    foreach ($deleteList as $id)
    {
        if (!preg_match("/^[0-9]+$/",$id))
        {
            cleanUp();
            die("WRONG! Some of the IDs to delete were not valid numbers!");
        }
    }

    foreach ($metaInfo as $field)
    {
        if (isMultiselect($field))
        {
            $query = "";
            $where = "";
            $params = array();

            // Do a query on the mk_table with the current pk
            foreach ($deleteList as $id)
            {
                if (strlen($where) != 0)
                {
                    $where .= "OR ";
                }
                $where .= $field['optional_args']['mk_main_pk'] . "=? ";

                $params[] = $id;
            }

            if (strlen($where)>0)
            {
                $query = "DELETE FROM " . $field['optional_args']['mk_table'] . " WHERE $where";

                $result = runReturnlessQuery($query, $params);
                if (!$result)
                {
                    cleanUp();
                    die("ERROR: Couldn't execute multikey query $query");
                }
                else
                {
                    print "Multikey table updated<br />";
                }
            }
        }
    }

    $where = "";
    $query = "";
    $params = array();

    foreach ($deleteList as $id)
    {
        if (strlen($where) != 0)
        {
            $where .= "OR ";
        }
        $where .= $pk_field . "=? ";

        $params[] = $id;
    }

    $query = "DELETE FROM $table WHERE $where";

    $result = runReturnlessQuery($query, $params);
    if (!$result)
    {
        cleanUp();
        die("ERROR: Couldn't delete rows with query $query");
    }
    else
    {
        print "Records deleted";
    }
}

/************************************************************************
* This function inspects the database and gets all the column names     *
* along with what data type they are                                    *
************************************************************************/
/*
function loadTableColumnTypes($table)
{
    global $postgresColumnTypes, $columnTypes;

    if (!array_key_exists($table, $columnTypes))
    {
        if (!startMultiRowLookup("SELECT column_name, data_type FROM information_schema.columns WHERE table_name='lists'"))
        {
            print "Couldn't execute query, last error: " . getLastError();
        }

        while ($row = getNextRowAssoc())
        {
            if (array_key_exists($row['data_type'], $postgresColumnTypes))
            {
                $columnTypes[$table][$row['column_name']] = $postgresColumnTypes[$row['data_type']];
            }
            else
            {
                print "WARNING: Could not understand datatype of column " .
                        $row['column_name'] . " with type " . $row['data_type'] . "<br />\n";
            }
        }
    }

    return true;
}
*/

/************************************************************************
* This function reads in all the values of a metafile and returns       *
* a multidimensional array. The top level array being numerical and     *
* an associative array per field inside that.                           *
*                                                                       *
*     (   0 =>    (                                                     *
*             field=>'asset_tag',                                       *
*             name=>'Asset Tag',                                        *
*             form_type=>'text',                                        *
*             validation_type=>'alphanumeric_mixed_case',               *
*             required=>TRUE,                                           *
*             optional_args=>NULL                                       *
*         ),                                                            *
*         1 =>    (                                                     *
*             field=>'vendor',                                          *
*             name=>'Vendor',                                           *
*             form_type=>'dropdown_lookup_vendors',                     *
*             validation_type=>'numeric',                               *
*             required=>TRUE,                                           *
*             optional_args=>(                                          *
*                 lookup_table=>vendors,                                *
*                 dropdown_key=>id,                                     *
*                 dropdown_val=>name                                    *
*             )                                                         *
*         )                                                             *
*     )                                                                 *
*                                                                       *
************************************************************************/
function metaFileExists($name) {
    global $_FORMLIB_CONFIG_PATH;

    $curdir = getcwd();

    if (!chdir("$_FORMLIB_CONFIG_PATH/metafiles")) {
        die("Couldn't change dir into metafiles directory");
    }

    $metafiles = glob("*.meta");

    if (!chdir($curdir)) {
        die("Couldn't change dir back to original after reading metafiles");
    }

    if (in_array($name . '.meta', $metafiles, true)) {
        return true;
    }

    return false;
}

function readMetaFile($defaultPage) {
    global $validation_types, $table, $pk_field, $_FORMLIB_CONF, $FORM_TYPES, $metaInfo, $file, $multikey, $list_order, $view, $_FORMLIB_CONFIG_PATH;

    if (!isset($_REQUEST['page']))
    {
        $file = $defaultPage;
    }
    else
    {
        $file = $_REQUEST['page'];
    }

    $count = 0;

    $filepath = "$_FORMLIB_CONFIG_PATH/metafiles/" . $file . ".meta";

    if (preg_match('/^[-_0-9A-Za-z]{3,20}$/',$file) && metaFileExists($file) && file_exists($filepath))
    {
        $fh = fopen($filepath,'r');
        // First we need to read the first line to tell us which table we are working on
        // Skip lines while they are a comment
        $count++;
        $line = chop(fgets($fh,1024));
        while (strcmp($line[0],"#") == 0)
        {
            $count++;
            $line = fgets($fh,1024);
        }

        $line = trimComment($line);

        // Line is TABLE=whatever;PK=<primary_key_column_name>;LIST_ORDER=<columns_to_sort_on>
        $fields = explode(";",$line);
        // Get the TABLE name
        $array = explode("=",$fields[0]);
        // If we dont end up with 2 elements, the first of which is TABLE
        // and the second of which is alpha numeric, no spaces with _and - allowed then die
        if (count($array) != 2 || strcmp($array[0],"TABLE") !=0 || !preg_match("/^[A-Za-z0-9_-]+$/",$array[1]))
        {
            die("Couldn't understand the first non-commented line of metafiles/" . $file .
                ".meta, format should be TABLE=<table_name>,PK=<primary_key_field> on line $count");
        }
        $table = $array[1];

        //loadTableColumnTypes($table);

        // Get the Primary Key field
        $array = explode("=",trim($fields[1]));
        // If we dont end up with 2 elements, the first of which is PK and the
        // second of which is alpha numeric, no spaces with _and - allowed then die
        if (count($array) != 2 || strcmp($array[0],"PK") !=0 || !preg_match("/^[A-Za-z0-9_-]+$/",$array[1]) )
        {
            die("Couldn't understand the first non-commented line of metafiles/" . $file .
                ".meta, format should be TABLE=<table_name>,PK=<primary_key_field> on line $count");
        }
        $pk_field = $array[1];

        // Get the List Order field
        $array = explode("=",trim($fields[2]));
        // If we dont end up with 2 elements, the first of which is PK and the
        // second of which is alpha numeric, no spaces with _and - allowed then die
        if (count($array) != 2 || strcmp($array[0],"LIST_ORDER") !=0 || !preg_match("/^[A-Za-z0-9,_-]+$/",$array[1]) )
        {
            die("Couldn't understand the first non-commented line of " . $file .
                ".meta, format should be TABLE=<table_name>,PK=<primary_key_field> on line $count");
        }
        $list_order = $array[1];

        // If the thrid field is set then it specifies a VIEW to use for displaying the data
        if (isset($fields[3]))
        {
            $array = explode("=",trim($fields[3]));

            if (count($array) != 2 || strcmp($array[0],"VIEW") !=0 || !preg_match("/^[A-Za-z0-9,_-]+$/",$array[1]) )
            {
                die("Couldn't understand the first non-commented line of metafiles/" . $file .
                    ".meta, format should be TABLE=<table_name>,PK=<primary_key_field> on line $count");
            }
            $view = $array[1];
        }

        $dependencies = array();

        // Now for each further line
        while ($line = fgets($fh,1024))
        {
            if (++$count >= 500)
            {
                die ("500 or more lines in file metafiles/" . $file . ".meta, sounds broken to me, exiting");
            }

            // If the line is a comment line skip
            if (strcmp($line[0],"#") == 0)
            {
                continue;
            }

            $line = trimComment($line);

            // explode on : into an assoc array
            $array = explode(":",$line,6);
            if (count($array)!=6)
            {
                die("FILE metafiles/" . $file . ".meta does not have the required " .
                    "number of arguments on line $count\n<br /><br />\n$line");
            }

            $metaPlaceholder = array();
            $field = $array[0];
            $name = $array[1];
            $form_type = $array[2];
            $validation_type = $array[3];
            $required = $array[4];
            $optional_args_placeholder = chop($array[5]);
            $optional_args = array();

            // validate the field names
            if (!preg_match("/^[A-Za-z0-9_-]+$/",$field))
            {
                die("FIELD NAME $field IN FILE metafiles/" . $file . ".meta IS NOT VALID - line $count");
            }
            // validate the real names
            if (!preg_match("/^[ \/A-Za-z0-9_-]+$/",$name))
            {
                die("DESCRIPTIVE NAME $name IN FILE metafiles/" . $file .
                    ".meta IS NOT VALID FOR FIELD $field - line $count");
            }

            // validate the form types
            if (!in_array($form_type,$FORM_TYPES))
            {
                die("FORM TYPE $form_type IN FILE metafiles/" . $file .
                    ".meta IS NOT VALID FOR FIELD $field - line $count");
            }
            // If it is a multikey type set the multikey flag
            if (strstr($form_type,"multiselect"))
            {
                $multikey = true;
            }

            // validate that the validation types exist
            if (!array_key_exists($validation_type,$validation_types))
            {
                die("VALIDATION TYPE $validation_type IN FILE metafiles/" . $file .
                    ".meta IS NOT VALID FOR FIELD $field - line $count");
            }

            // If required does not equal true or false
            if (strcasecmp($required,"true") != 0 && strcasecmp($required,"false") != 0)
            {
                die("REQUIRED FIELD IS NOT true OR false IN FILE metafiles/" . $file .
                    ".meta FOR FIELD $field - line $count");
            }

            // If there are optional args explode into k=>v pairs and then make these into an assoc array
            if ($optional_args_placeholder != null && strlen($optional_args_placeholder) > 0)
            {
                $pairs = explode(";",$optional_args_placeholder);
                foreach($pairs as $pair)
                {
                    $arg = explode("=",$pair,2);
                    if (count($arg)!=2)
                    {
                        die("OPTIONAL ARG $arg IN FILE metafiles/" . $file .
                            ".meta FOR FIELD $field IS NOT VALID - line $count");
                    }
                    else
                    {
                        $optional_args[$arg[0]] = $arg[1];
                        // If this arg says this field is dependant on another we should put the name
                        // of the other into the dependancy array
                        if (strcmp($arg[0],"parent") == 0)
                        {
                            $dependencies[] = $arg[1];
                        }
                    }
                }
            }

            // If it was a lookup type validate that the needed optional args were there
            if (strpos($form_type, 'lookup')) {
                // All lookups need table, key and val optional args
                if (!array_key_exists("table", $optional_args)) {
                    die("ERROR in metafile line $count. lookup types must provide table, key and val optional args. table missing");
                }

                if (!array_key_exists("key", $optional_args)) {
                    die("ERROR in metafile line $count. lookup types must provide table, key and val optional args. key missing");
                }

                if (!array_key_exists("val", $optional_args)) {
                    die("ERROR in metafile line $count. lookup types must provide table, key and val optional args. val missing");
                }
            }

            // If it was a multiselect type validate that all the mk optional args are present
            if (strpos($form_type, 'multiselect')) {
                // All multiselect need mk_table, mk_main_pk, mk_list_pk
                if (!array_key_exists("mk_table", $optional_args)) {
                    die("ERROR in metafile line $count. multikey types must provide mk_table, mk_main_pk and mk_list_pk optional args. mk_table missing");
                }

                if (!array_key_exists("mk_main_pk", $optional_args)) {
                    die("ERROR in metafile line $count. multikey types must provide mk_table, mk_main_pk and mk_list_pk optional args. mk_main_pk missing");
                }

                if (!array_key_exists("mk_list_pk", $optional_args)) {
                    die("ERROR in metafile line $count. multikey types must provide mk_table, mk_main_pk and mk_list_pk optional args. mk_list_pk missing");
                }
            }

            // push this array onto the top array
            $metaPlaceholder["field"] = $field;
            $metaPlaceholder["name"] = $name;
            $metaPlaceholder["form_type"] = $form_type;
            $metaPlaceholder["validation_type"] = $validation_type;
            $metaPlaceholder["required"] = $required;
            $metaPlaceholder["optional_args"] = $optional_args;

            $metaInfo[] = $metaPlaceholder;
        }

        // Mark fields which are dependencies as such
        foreach ($metaInfo as &$field)
        {
            if (in_array($field['field'],$dependencies))
            {
                $field['optional_args']['dependency'] = true;
            }
        }

        // If metaInfo is empty then it means there were no lines to read so die
        if (count($metaInfo) == 0)
        {
            die("ERROR file metafiles/" . $file . ".meta is empty");
        }
    }
    else
    {
        die("The page name you specified was not valid");
    }
}
/***********************************************************************/

/************************************************************************
* This function will perform the database lookups for all of the        *
* types which require it                                                *
************************************************************************/
function populateLookups()
{
    global $metaInfo, $lookups;

    foreach ($metaInfo as $field)
    {
        // If the current field is of a lookup type
        if (strstr($field['form_type'],"lookup"))
        {
            $where = "";
            $params = array();

            // If this has a parent
            if (hasParent($field))
            {
                if (isset($_POST[$field['optional_args']['parent']]) && strlen($_POST[$field['optional_args']['parent']]) > 0)
                {
                    if (strlen($_POST[$field['optional_args']['parent']]) != 0)
                    {
                        $where .= " WHERE " . $field['optional_args']['parent'] . "=?";
                        $params[] = $_POST[$field['optional_args']['parent']];
                    }
                }
                else
                {
                    for ($i = 0;$i<count($metaInfo);$i++)
                    {
                        if (strcmp($metaInfo[$i]['field'],$field['optional_args']['parent']) == 0
                            && ($metaInfo[$i]['value'] != NULL && strcmp($metaInfo[$i]['field'],"NULL")!=0))
                        {
                            $where .= " WHERE " . $field['optional_args']['parent'] . "=?";
                            $params[] = $metaInfo[$i]['value'];
                        }
                    }
                }
            }

            $query = "SELECT " . $field['optional_args']['key']  . "," . $field['optional_args']['val']
                . " FROM " . $field['optional_args']['table'] . $where
                . " ORDER BY " . $field['optional_args']['val'];


            $result = startMultiRowLookup($query, $params);
            if (!$result)
            {
                print "ERROR: Couldn't perform a lookup on the database for query $query";
                cleanUp();
                die();
            }
            while ($row = getNextRow())
            {
                $lookups[$field['field']][$row[0]] = $row[1];
            }
        }
        else
        {
            switch($field['form_type'])
            {
                case "dropdown":
                case "list":
                case "list_multiselect":
                case "radio":
                    foreach($field['optional_args'] as $k=>$v)
                    {
                        // In the special circumstance of list multiselects
                        // we need to ignore all the args that start mk_ as these
                        // are the multikey options
                        if (!preg_match("/^mk_/",$k))
                        {
                            $lookups[$field['field']][$k] = $v;
                        }
                    }
                    break;
            }
        }
    }
}
/***********************************************************************/

/************************************************************************
* Basic database functions                                              *
************************************************************************/

function insertRecord()
{
    global $multikey, $pk_field;

    // Do the main insert into the database (this wont include any multikey inserts)
    list( $ins_query, $ins_params ) = generateSingleKeyInsertQueryWithParams();

    $inserted_pk = runInsertReturningID($ins_query, $ins_params, $pk_field);

    if ($inserted_pk !== false) {
        print "The entry was added to the database<br />";
    }
    else {
        print "ERROR: Couldn't add the entry to the database.<br />\nQUERY: " . $ins_query . "<br />\nDB ERROR: ";
        var_dump(getLastError());
    }

    // If we have multikey elements
    if ($multikey) {
        // Now we need to do the multikey inserts, these are inserts
        // into a separate table which is an intermidiary
        // in a many to many table join
        list($queries, $params) = generateMultiKeyInsertQueryWithParams($inserted_pk);

        $errors = array();
        $i = 0;
        foreach ($queries as $query) {
            if (!runReturnlessQuery($query, $params[$i])) {
                $errors[] = getLastError();
            }

            $i++;
        }

        if (count($errors)) {
            foreach ($errors as $error) {
                print "ERROR: Couldn't add the entry to the database.<br />\nDatabase said: " . $error;
            }
        }
        else {
            print "Many to Many Foreign key relationships added<br />";
        }
    }
}

function updateRecord()
{
    global $multikey;

    // Do the main update on the database (this wont include any multikey updates)
    list($query, $params) = generateSingleKeyUpdateQueryWithParams();
    if (runReturnlessQuery($query, $params)) {
        print "The entry was updated<br />";
    }
    else {
        print "ERROR: Couldn't update the entry to the database.<br />\nDatabase said: " . getLastError();
    }

    // If we have multikey elements
    if ($multikey)
    {
        // Now we need to do the multikey inserts, these are inserts
        // into a separate table which is an intermidiary
        // in a many to many table join
        list($queries, $params) = generateMultiKeyUpdateQueryWithParams();

        $errors = array();
        $i = 0;
        foreach ($queries as $query) {
            if (!runReturnlessQuery($query, $params[$i])) {
                $errors[] = getLastError();
            }
            $i++;
        }

        if (count($errors))
        {
            foreach ($errors as $error)
            {
                print "ERROR: Couldn't update entries in the database.<br />\nDatabase said: " . $error;
            }
        }
        else
        {
            print "Many to Many Foreign key relationships updated<br />";
        }
    }
}

/***********************************************************************/

/************************************************************************
* This function loads all the required files and validates the          *
* various meta files that are loaded                                    *
************************************************************************/
function formlib_init()
{
    global $validation_types,$_FORMLIB_CONF,$_FORMLIB_CONFIG_PATH;

    require("$_FORMLIB_CONFIG_PATH/formlib.conf");

    // Load the validation types
    $fh = fopen("$_FORMLIB_CONFIG_PATH/validation_types.conf",'r') or die("Couldn't open the validation types config");
    while ($line = fgets($fh,'1024'))
    {
        // If it is a comment line
        if (strcmp($line[0],"#") == 0)
        {
            // Skip to the next line
            continue;
        }

        $line = trimComment($line);

        $conf = explode(":",$line,2);
        $validation_types[$conf[0]] = $conf[1];
    }
    fclose($fh);
}

function trimComment($string)
{
    // If there is a comment on the end of the line, truncate the line
    $comment_start = strpos($string, '#');

    if ($comment_start === false) {
        return trim($string);
    }

    // Remove the comment from the end
    $string = substr($string, 0, $comment_start);

    return trim($string);
}

function generateSingleKeyInsertQueryWithParams()
{
    global $metaInfo,$pk_field,$table;

    $query = "";
    $params = array();

    $query_field_names = array();
    $query_values = array();

    foreach ($metaInfo as $field)
    {
        // We need to ignore the field that is the PK field since we are
        // inserting a new row and NOT updating an existing one
        // We also need to ignore multiselect fields since they interact with
        // many to many join tables and don't have a field in the main table

        // IF field IS NOT (primaryKeyField OR isMultiSelect)
        if (!(strcmp($field['field'],$pk_field) == 0 || isMultiselect($field))
            && !viewOnly($field))
        {
            $includeField = false;

            // If it is not numeric we need to enclose it in quotes
            // If the field is a boolean (checkbox) we need to rewrite to TRUE or FALSE
            // although we use 1 and 0 for mysql compatability
            if (strcmp($field['form_type'],"checkbox") == 0)
            {
                $includeField = true;
                $query_values[] = "?";
                if ($_POST[$field['field']] == 1) {
                    $params[] = '1';
                } else {
                    $params[] = '0';
                }
            }
            elseif (is_numeric($_POST[$field['field']]))
            {
                $includeField = true;
                $query_values[] = "?";
                $params[] = $_POST[$field['field']];
            }
            else
            {
                if ($_POST[$field['field']] != null)
                {
                    # If the field is empty and it's not required move on to the next one
                    if ($_POST[$field['field']] == '' && !required($field))
                    {
                        continue;
                    }

                    $includeField = true;

                    if (strcmp($field['validation_type'],"numeric") == 0)
                    {
                        $query_values[] = "?";
                        $params[] = 'NULL';
                    }
                    else
                    {
                        $query_values[] = "?";
                        $params[] = $_POST[$field['field']];
                    }
                }
            }

            if ($includeField)
            {
                $query_field_names[] .= $field['field'];
            }
        }
    }
    $query = "INSERT INTO $table (" . join(',', $query_field_names) . ") VALUES (". join(',',$query_values) .")";

    return array($query, $params);
}

function generateMultiKeyInsertQueryWithParams($insert_key)
{
    global $metaInfo, $table;

    $queries = array();
    $params = array();

    // FOR EACH multikey field that has values returned to us
    foreach ($metaInfo as $field)
    {
        if (($_POST[$field['field']] && isMultiselect($field))
            && !viewOnly($field))
        {
            $query = "";
            $query_params = array();

            $args = $field['optional_args'];
            $query = "INSERT INTO " . $args['mk_table'] . " (" . $args['mk_main_pk'] . "," . $args['mk_list_pk']  . ") VALUES ";
            $first = true;
            foreach ($_POST[$field['field']] as $value)
            {
                if (!$first)
                {
                    $query .= ",";
                }
                $query .= "(?,?) ";
                $query_params[] = intval($insert_key);
                $query_params[] = intval($value);

                $first = false;
            }
            $queries[] = $query;
            $params[] = $query_params;
        }
    }

    return array($queries, $params);
}

function generateSingleKeyUpdateQueryWithParams()
{
    global $metaInfo,$pk_field,$table;

    $query = "UPDATE $table SET ";
    $params = array();

    $firstVar = true;

    foreach ($metaInfo as $field)
    {
        // We need to ignore the field that is the PK field since we are
        // inserting a new row and NOT updating an existing one
        // We also need to ignore multiselect fields since they interact with
        // many to many join tables and don't have a field in the main table

        // IF field IS NOT (primaryKeyField OR isMultiSelect OR view_only)
        if (!( strcmp($field['field'],$pk_field) == 0
            || isMultiselect($field)
            || viewOnly($field)))
        {
            $includeField = false;
            $appendString = "";

            // If it isn't the first var then we need to add a comma delimiter
            if (!$firstVar)
            {
                $appendString .= ",";
            }
            else
            {
                $firstVar = false;
            }

            // If it is not numeric we need to enclose it in quotes
            // If the field is a boolean (checkbox) we need to rewrite to TRUE or FALSE
            // although we use 1 and 0 for mysql compatability
            if (strcmp($field['form_type'],"checkbox") == 0)
            {
                $includeField = true;
                if ($_POST[$field['field']] == 1)
                {
                    $appendString .= $field['field'] . "=?";
                    $params[] = '1';
                }
                else
                {
                    $appendString .= $field['field'] . "=?";
                    $params[] = '0';
                }
            }
            elseif (is_numeric($_POST[$field['field']]))
            {
                $includeField = true;
                $appendString .= $field['field'] . "=?";
                $params[] = $_POST[$field['field']];
            }
            else
            {
                if ($field['field'] != null)
                {
                    # If the field is empty and it's not required move on to the next one
                    if ($_POST[$field['field']] == '' && !required($field))
                    {
                        continue;
                    }

                    $includeField = true;

                    if (strcmp($field['validation_type'],"numeric") == 0)
                    {
                        $appendString .= $field['field'] . "=?";
                        $params[] = 'NULL';
                    }
                    else
                    {
                        $appendString .= $field['field'] . "=?";
                        $params[] = $_POST[$field['field']];
                    }
                }
            }

            if ($includeField)
            {
                $query .= $appendString;
            }
        }
    }

    $query .= " WHERE $pk_field=?";
    $params[] = $_POST[$pk_field];

    return array($query, $params);
}

function generateMultiKeyUpdateQueryWithParams()
{
    global $metaInfo,$table,$pk_field;

    $queries = array();
    $params = array();

    // FOR EACH multikey field (ones with no values returned need their entries in the mk table deleting)
    foreach ($metaInfo as $field)
    {
        if (isMultiselect($field) && !viewOnly($field))
        {
            $query = "";
            $query_params = array();

            $args = $field['optional_args'];

            // Even when we are just updating we need to remove all entries from the MK table
            $queries[] = "DELETE FROM " . $args['mk_table'] .
                    " WHERE " . $args['mk_main_pk'] . "=?";
            $params[] = array($_POST[$pk_field]);

            // If values have been returned then we need to insert them into the mk table
            // after our previous deletion
            if ($_POST[$field['field']])
            {
                $query = "INSERT INTO " . $args['mk_table']
                    . " (" . $args['mk_main_pk'] . "," . $args['mk_list_pk']
                    . ") VALUES ";
                $first = true;
                foreach ($_POST[$field['field']] as $value)
                {
                    if (!$first)
                    {
                        $query .= ", ";
                    }

                    $query .= "(?,?)";
                    $query_params[] = intval($_POST[$pk_field]);
                    $query_params[] = intval($value);

                    $first = false;
                }
                $queries[] = $query;
                $params[] = $query_params;
            }
        }
    }
    return array($queries, $params);
}

function cleanUp()
{
    disconnectFromDatabase();
}
/***********************************************************************/

/************************************************************************
* This series of functions handle the displaying of form elements       *
* to the page and the populating with data                              *
************************************************************************/
function displayHiddenField($field)
{
    global $mode, $modes;

    $readonly = "";

    if (viewOnly($field) && !searchMode())
    {
        $readonly = "readonly='readonly'";
    }
    print "\t<input $readonly type='hidden' name='" . $field['field'] . "' value='" . cleanValue($field) . "' />\n";
}

function displayTextField($field)
{
    global $mode, $modes;

    $readonly = "";

    if (viewOnly($field) && !searchMode())
    {
        $readonly = "readonly='readonly'";
    }

    print "<input $readonly name='" . $field['field'] . "' value='" . cleanTextForInput(cleanValue($field)) . "' />";
}

function displayDropdownField($field)
{
    global $mode, $modes, $lookups;


    $javascript = "";

    if (hasDependency($field))
    {
        $javascript = " onchange='getElementById(\"resolve_dependency\").click()'";
    }

    $readonly = "";

    if (viewOnly($field) && !searchMode())
    {
        $readonly = "readonly='readonly'";
    }
    print "\n\t\t<select $readonly name='" . $field['field'] . "'$javascript>\n";

    // If it is not a required field we should show an option that returns no value as well
    if (!required($field) || searchMode())
    {
        print "\t\t\t<option value=''>----</option>\n";
    }
    // If the lookup array for this field name has been populated
    if (isset($lookups[$field['field']]))
    {
        // Then for each item in the lookup array print an option
        foreach ($lookups[$field['field']] as $key=>$val)
        {
            $selected = "";
            // If the key for this option is the same as the value in the db for this row
            // then set it to selected
            if ($key == cleanValue($field))
            {
                $selected = "selected='selected'";
            }

            print "\t\t\t<option $selected value='$key'>" . cleanTextForInput($val) . "</option>\n";
        }
    }
    print "\t\t</select>\n";
    if (hasDependency($field) && !viewOnly($field))
    {
        print "<input type='submit' id='resolve_dependency'  name='resolve_dependency' value='Select' />";
    }
}

function displayListField($field)
{
    global $mode, $modes, $lookups;

    if (viewOnly($field) && !searchMode())
    {
        print "\n\t\t<select readonly='readonly' size='8' name='" . $field['field'] . "'>\n";
    }
    else
    {
        print "\n\t\t<select size='8' name='" . $field['field'] . "'>\n";
    }
    // If the lookup array for this field name has been populated
    if (isset($lookups[$field['field']]))
    {
        // Then for each item in the lookup array print an option
        foreach ($lookups[$field['field']] as $key=>$val)
        {
            $selected = "";
            // If the key for this option is the same as the value in the db for this row
            // then set it to selected
            if ($key == cleanValue($field))
            {
                $selected = "selected='selected'";
            }

            print "\t\t\t<option $selected value='$key'>" . cleanTextForInput($val) . "</option>\n";
        }
    }
    print "\t\t</select>\n";
}

function displayMultiselectListField($field)
{
    global $mode, $modes, $lookups;

    $readonly = "";

    if (viewOnly($field) && !searchMode())
    {
        $readonly = "readonly='readonly'";
    }
    print "\n\t\t<select $readonly size='8' multiple='multiple' name='" . $field['field'] . "[]'>\n";

    // If the lookup array for this field name has been populated
    if (isset($lookups[$field['field']]))
    {
        // Then for each item in the lookup array print an option
        foreach ($lookups[$field['field']] as $key=>$val)
        {
            $selected = "";

            // If the array $field['value'] contains $key then show it as selected
            if (is_array(cleanValue($field)) && in_array($key,$field['value']))
            {
                $selected = "selected='selected'";
            }

            print "\t\t\t<option $selected value='$key'>" . cleanTextForInput($val) . "</option>\n";
        }
    }
    print "\t\t</select>\n";
}

function displayCheckboxField($field)
{
    global $mode, $modes;

    $checked = "";
    $readonly = "";

    if (searchMode()) {
        print "<select name='{$field['field']}'>\n";
        print " <option value=''>--</option>\n";
        print " <option value='true'>True</option>\n";
        print " <option value='false'>False</option>\n";
        print "</select>\n";
    }
    else {
        if (cleanValue($field) == 1) {
            $checked = "checked='checked'";
        }

        if (viewOnly($field)) {
            $readonly = "readonly='readonly'";
        }

        print "<input $readonly type='checkbox' name='" . $field['field'] .
                "' value='" . $field['field'] . "' $checked />";
    }
}

function displayRadioField($field)
{
    global $mode, $modes, $lookups;

    // If the lookup array for this field name has been populated
    if (isset($lookups[$field['field']]))
    {
        print "\n";
        // Then for each item in the lookup array print an option
        foreach ($lookups[$field['field']] as $key=>$val)
        {
            $readonly = "";
            $checked = "";
            // If the key for this option is the same as the value in the db for this row
            // then set it to selected
            if ($key == cleanValue($field))
            {
                $checked = "checked='checked'";
            }

            if (viewOnly($field) && !searchMode())
            {
                $readonly = "readonly='readonly'";
            }

            print "\t\t<input $readonly $checked type='radio' " .
                    "name='" . $field['field'] . "' value='$key' />" . cleanTextForInput($val) . "\n";
        }
    }
}

function displayTextareaField($field)
{
    global $mode, $modes;

    $readonly = "";

    if (viewOnly($field) && !searchMode())
    {
        $readonly = "readonly='readonly'";
    }

    print "<textarea $readonly rows='5' cols='30'  name='" . $field['field'] . "'>" . cleanTextForInput(cleanValue($field)) . "</textarea>";
}

function displayFormHeader()
{
    global $file,$mode,$modes,$_FORMLIB_CONF,$lookup_key;

    $script = explode("/",$_SERVER["SCRIPT_NAME"]);

    print "<form method='post' action='" . array_pop($script) . "?page=$file'>\n";

    print $_FORMLIB_CONF['preform'];

    // We need to output a hidden field declaring which mode we will need when the form is submitted
    switch($mode)
    {
        case $modes['new']:
            // If we are in new we want to insert a new row
            print "\t<input type='hidden' name='mode' value='insert' />\n";
            break;
        case $modes['edit']:
            // If we are in edit we need to move into update mode
            print "\t<input type='hidden' name='mode' value='update' />\n";
            // We also need to pass on the lookup key since it came from a get and needs to be in the post
            print "\t<input type='hidden' name='pk' value='$lookup_key' />\n";
            break;
        case $modes['insert']:
            // If we are in insert and have got this far it means that there were errors
            // and as such we still want to insert
            print "\t<input type='hidden' name='mode' value='insert' />\n";
            break;
        case $modes['update']:
            // If we are in update and have got this far it means that there were errors
            // and as such we still want to update
            print "\t<input type='hidden' name='mode' value='update' />\n";
            break;
        case $modes['search']:
            print "\t<input type='hidden' name='search' value='1' />\n";
            print "\t<input type='hidden' name='page' value='$file' />\n";
            print "\t<input type='hidden' name='mode' value='list' />\n";
            break;
        case $modes['delete']:
            break;
        default:
            die("ERROR: Could not understand what mode to be in");
            break;
    }
}

function displayPreField($field)
{
    global $_FORMLIB_CONF;
    // If it is a hidden field we don't want to print anything
    if (!$field || !hiddenField($field))
    {
        print $_FORMLIB_CONF['prefield'];
    }
}

function displayLabel($field)
{
    global $_FORMLIB_CONF;

    print $_FORMLIB_CONF['prelabel'];
    // If $field is null then we want to display no label
    if ($field)
    {
        if (required($field)) print "* ";
        print $field['name'] ;
    }
    else
    {
        print "&nbsp;";
    }
    print $_FORMLIB_CONF['postlabel'];
}

function displayField($field)
{
    displayPreField($field);
    // If $field is null then we want to do the submit field
    if ($field)
    {
        switch ($field['form_type'])
        {
            case "hidden":
                displayHiddenField($field);
                break;
            case "text":
                displayTextField($field);
                break;
            case "dropdown":
            case "dropdown_lookup":
                displayDropdownField($field);
                break;
            case "list":
            case "list_lookup":
                displayListField($field);
                break;
            case "list_multiselect":
            case "list_multiselect_lookup":
                displayMultiselectListField($field);
                break;
            case "checkbox":
                displayCheckboxField($field);
                break;
            case "radio":
            case "radio_lookup":
                displayRadioField($field);
                break;
            case "textarea":
                displayTextareaField($field);
                break;
        }
    }
    else
    {
        print "<input type='submit' value='Submit' />";
    }
    displayPostField($field);
}

function displayError($field)
{
    global $_FORMLIB_CONF;

    print $_FORMLIB_CONF['preerror'];
    // Otherwise if the error field for this field is set then print the error
    if (isset($field['error']))
    {
        print $field['error'];
    }
    else
    {
        print "&nbsp;";
    }
    print $_FORMLIB_CONF['posterror'];
}

function displayPostField($field)
{
    global $_FORMLIB_CONF;
    // If it is a hidden field we don't want to print anything
    if (!hiddenField($field))
    {
        print $_FORMLIB_CONF['postfield'];
    }
}

function displayFormFooter()
{
    global $_FORMLIB_CONF;
    print $_FORMLIB_CONF['postform'];
    print "</form>\n";
}
/***********************************************************************/

/************************************************************************
* This series of functions are just general helper functions            *
************************************************************************/
function getSearchExpression()
{
    global $metaInfo,$validation_types;

    if (isset($_REQUEST['search'])) {
        $searchField = $_REQUEST['search'];
    }
    else {
        $searchField = false;
    }

    $joinTables = array();

    $searchSQL = "";
    $params = array();
    $searchLink = "";

    if ($searchField)
    {
        $first = true;
        // We need to search through the list of metaFields and find the one that is being searched on
        foreach ($metaInfo as $field)
        {
            $varsToProcess = array();

            if (isMultiselect($field) && is_array($_REQUEST[$field['field']]))
            {
                # TODO FIXME - join these tables in....
                $joinTables[$field['optional_args']['mk_table']] = $field['optional_args']['mk_table'] . "." . $field['optional_args']['mk_main_pk'];

                foreach ($_REQUEST[$field['field']] as $listItem)
                {
                    $varsToProcess[] = cleanUrlText($listItem);
                }
            }
            else
            {
                $varsToProcess[] = cleanUrlText($_REQUEST[$field['field']]);
            }

            foreach ($varsToProcess as $var)
            {
                if (preg_match("/.+/",$var))
                {
                    list($searchString, $sqlString, $searchParams) = processSearchField($field, $var);

                    $searchLink .= $searchString;

                    # TODO FIXME - need to make multikey options joined on OR and in brackets
                    if ($first)
                    {
                        $searchSQL = "WHERE ";
                        $first = false;
                    }
                    else
                    {
                        $searchSQL .= "AND ";
                    }

                    $searchSQL .= $sqlString;
                    $params = array_merge($params, $searchParams);
                }
            }
        }
    }
    if (strlen($searchLink) > 0)
    {
        $searchLink = "search=on&amp;" . $searchLink;
    }
    return array($searchSQL, $searchLink, $joinTables, $params);
}

function processSearchField($field, $var)
{
    $searchSQL = "";
    $params = array();

    #TODO FIXME fix the searchLink generation when there are multikey fields
    $searchLink = $field['field'] . "=" . $var . "&amp;";

    $columnName = $field['field'];
    if (isMultiselect($field))
    {
        $columnName = $field['optional_args']['mk_table'] . "." . $field['optional_args']['mk_list_pk'];
    }

    if (strcmp($field['form_type'],"checkbox") == 0) {
        if ($var === "true") {
            $searchSQL .= $columnName . "=?";
            $params[] = 1;
        }
        elseif ($var === "false") {
            $searchSQL .= $columnName . "=?";
            $params[] = 0;
        }
    }
    else
    {
        if (is_numeric($var)) {
            $searchSQL .= " $columnName=? ";
        }
        else {
            // We allow wild card searching with % or * so convert *s to %s
            $var = str_replace("*","%",$var);
            $searchSQL .= "UPPER($columnName) LIKE UPPER(?) ";
        }
        $params[] = $var;
    }

    return array ($searchLink, $searchSQL, $params);
}

function getList($deleteMode = false)
{
    global $list,$metaInfo,$table,$list_order,$file,$pk_field,$lookups,$view;

    $sortorder = "";

    if (isset($_REQUEST['_formlib_sort']) && preg_match("/^[A-Za-z0-9_-]+$/",$_REQUEST['_formlib_sort'])
    && preg_match("/^(asc|desc|)$/",$_REQUEST['_formlib_sort_order']) /*|| !$_GET['_formlib_sort_order'])*/)
    {
        if (strcmp($_GET['_formlib_sort_order'],"desc") == 0)
        {
            $sort = " ORDER BY " . $_GET['_formlib_sort'] . " DESC";
            $sortorder .= "_formlib_sort_order=asc&amp;";
        }
        else
        {
            $sort = " ORDER BY " . $_GET['_formlib_sort'] . " ASC";
            $sortorder .= "_formlib_sort_order=desc&amp;";
        }
    }
    else if (isset($list_order))
    {
        $sort = " ORDER BY " . $list_order;
    }

    // We DO NOT want to allow a search and a delete to occur at the same time
    if (isset($_REQUEST['search']) && $deleteMode)
    {
        die("ERROR: NO NO NO, NO SEARCH AND DELETE AT THE SAME TIME!!!!!!!!!");
    }

    list($searchSQL, $searchLink, $joinTables, $searchParams) = getSearchExpression();

    $where = "";
    $join = "";
    $params = $searchParams;

    foreach ($joinTables as $joinTable => $joinField)
    {
        $join .= "LEFT JOIN " . $joinTable . " ON $table.$pk_field = " . $joinField;
    }

    $where = $searchSQL;

    if ($deleteMode && strlen($where) == 0)
    {
        $deleteList = $_POST['_FORMLIB_delete'];

        if (count($deleteList) < 1)
        {
            print "ERROR: You didn't specify anything to delete!";
            return false;
        }

        $keys_to_delete = array();
        $delete_params = array();

        foreach ($deleteList as $delKey)
        {
            if (!preg_match("/^[0-9]+$/",$delKey)) {
                print "ERROR: Invalid delete key specified";
                return false;
            }
            else {
                $keys_to_delete[] = "?";
                $delete_params[] = $delKey;
            }
        }
        if (count($keys_to_delete) == 0)
        {
            print "ERROR: Something has gone very wrong, the where cluase in the delete SQL is empty. I am not willing to blanket delete so am choosing to die instead!!!";
            return false;
        }
        if (strlen($where) > 0) {
            $where .= " OR $pk_field IN (" . join(',', $keys_to_delete)  . ")";
        }
        else {
            $where = "WHERE $pk_field IN (" . join(',', $keys_to_delete)  . ")";
        }
        $params = array_merge($params, $delete_params);
    }

    $linkSearch = $sortorder . $searchLink;

    // Do the lookup and populate a table
    $query = "SELECT * FROM " . ($view?$view:$table) . " $join $where $sort";
    startMultiRowLookup($query, $params);

    $script = array_pop(explode("/",$_SERVER["SCRIPT_NAME"]));

    print "<form method='post' action='" . $script . "'>\n";
    print "<div><input type='hidden' name='page' value='$file' /></div>";
    print "<div><input type='hidden' name='mode' value='delete' /></div>";
    print "<table>\n";
    print "\t<tr>\n";


    foreach ($metaInfo as $field)
    {
        // If its not a hidden field and isnt set manually to hide
        if (!hiddenField($field) && !listHide($field))
        {
            // For each search
            print "\t\t<th class='tableLabel'><a class='descLink' href='$script?" . $linkSearch . "page=$file&amp;mode=list&amp;_formlib_sort=" . $field["field"] . "'>" . $field['name'] . "</a></th>\n";
        }
    }
    if (!$deleteMode)
    {
        print "\t<th class='delHeader'><input type='submit' value='Delete' /></th>\n";
    }
    print "\t</tr>\n";

    $delField = "";

    while ($row = getNextRowAssoc())
    {
        print "\t<tr>\n";
        if ($deleteMode)
        {
            $delField = "\t\t<input type='hidden' name='_FORMLIB_delete[]' value='" . $row[$pk_field] . "' />\n";
        }

        foreach ($metaInfo as $field)
        {
            if (!hiddenField($field) && !listHide($field))
            {
                print "\t\t<td>";
                if (isset($delField))
                {
                    print $delField;
                    $delField = null;
                }
                if (!$deleteMode)
                {
                    if (hasEditLink($field))
                    {
                        print "<a href='" . $script . "?page=$file&amp;mode=edit&amp;pk=" . $row[$pk_field] . "'>";
                    }
                    else if (hasExternalLink($field))
                    {
                        $url = $field["optional_args"]["ext_link"];
                        $url = str_replace("[field]",$row[$field['field']],$url);
                        print "<a target='_blank' class='external' href='$url'>";
                    }
                }

                switch ($field['form_type'])
                {
                    case "dropdown":
                    case "dropdown_lookup":
                    case "list":
                    case "list_lookup":
                    case "list_multiselect":
                    case "radio":
                    case "radio_lookup":
                        if (array_key_exists($row[$field['field']], $lookups[$field['field']])) {
                            print cleanUrlText($lookups[$field['field']][$row[$field['field']]]);
                        } else {
                            print "&nbsp;";
                        }
                        break;
                    case "checkbox":
                        if ($row[$field['field']] == "t" || $row[$field['field']] == 1)
                        {
                            print "True";
                        }
                        else
                        {
                            print "False";
                        }
                        break;
                    case "list_multiselect_lookup":
                        $query = "
                            SELECT t.$pk_field AS pk,
                                lt." . $field['optional_args']['val']  . " AS val
                            FROM $table AS t
                            JOIN " . $field['optional_args']['mk_table'] . " AS mt
                                ON t.$pk_field = mt." . $field['optional_args']['mk_main_pk'] . "
                            JOIN " . $field['optional_args']['table'] . " AS lt
                                ON mt." . $field['optional_args']['mk_list_pk'] . " = lt." . $field['optional_args']['key'] . "
                            WHERE t.$pk_field = ?";

                        $params = array($row[$pk_field]);

                        $stmt_handle = startLookupReturnHandle($query, $params);
                        if ($stmt_handle === false)
                        {
                            cleanUp();
                            die("ERROR: Unable to perform lookup query $query");
                        }

                        $output = "";

                        while ($multikey_lookup_row = $stmt_handle->fetch(PDO::FETCH_ASSOC))
                        {
                            if (strlen($output) > 0)
                            {
                                $output .= "\n-\n";
                            }
                            $output .= $multikey_lookup_row['val'];
                        }

                        print cleanUrlText($output);
                        break;
                    default:
                        print cleanUrlText($row[$field['field']]);
                        break;
                }
                if (!$deleteMode && hasEditLink($field) || (hasExternalLink($field)) )
                {
                    print "</a>";
                }
                /*else if (!$deleteMode && hasExternalLink($field))
                {
                    print "WE ARE HERE 2";
                    print "</a><img src='images/external.png' />";
                }*/
            print "</td>\n";
            }
        }
        if (!$deleteMode)
        {
            // Show the delete column
            print "<td><input type='checkbox' name='_FORMLIB_delete[]' value='" . $row[$pk_field] . "' /></td>\n";
        }
        print "\t</tr>\n";
    }
    print "</table>";
    if ($deleteMode)
    {
        print "<div><input type='submit' name='confirm' value='CONFIRM DELETION!!!' /></div>";
    }
    print "</form>";
}

function searchMode()
{
    global $mode, $modes;

    if ($mode == $modes['search']) return true;
    else return false;
}

function getMode($defaultMode) {
    global $mode, $modes;

    if (isset($_POST['mode']) && isset($modes[$_POST['mode']]))
    {
        $mode = $modes[$_POST['mode']];
    }
    elseif (isset($_GET['mode']) && isset($modes[$_GET['mode']]))
    {
        $mode = $modes[$_GET['mode']];
    }
    elseif (isset($defaultMode) && isset($modes[$defaultMode]))
    {
        $mode = $modes[$defaultMode];
    }


    // If we are resolving dependencies we want to keep the mode as whatever it was last time
    if (isset($_POST['resolve_dependency']))
    {
        switch ($mode)
        {
            case $modes['insert']:
                $mode = $modes['new'];
                break;
            case $modes['update']:
                $mode = $modes['edit'];
                break;
            case $modes['list']:
                $mode = $modes['search'];
                break;
        }
    }

}

function getLookupKey()
{
    global $lookup_key;

    if (isset($_POST['pk']))
    {
        $lookup_key = chop($_POST['pk']);
    }
    elseif (isset($_GET['pk']))
    {
        $lookup_key = chop($_GET['pk']);
    }

    if (!preg_match("/^[A-Za-z0-9_-]+$/",$lookup_key))
    {
        cleanUp();
        die("ERROR: the lookup key specified is invalid");
    }
}

function validateVars() {
    global $metaInfo,$validation_types;

    $errorFlag = false;

    foreach ($metaInfo as &$field)
    {
        // Checkboxes are special and need to be handled differently
        if (strcmp($field['form_type'],"checkbox") == 0)
        {
            // When we have a checkbox if it returns a value the same as the field name then
            // it is true, otherwise it is false
            //
            // If it is true we will set it to 1
            // If it is not we will set it to 0
            if (strcmp($_POST[$field['field']],$field['field']) == 0)
            {
                $_POST[$field['field']] = 1;
            }
            else
            {
                $_POST[$field['field']] = 0;
            }
        }
        else
        {
            // All other fields can be validated against their validation type
            if (required($field) && !$_POST[$field['field']])
            {
                $field['error'] = $field['name'] . " is required";
                $errorFlag = true;
            }

            // If the field is allowed to be empty and it is either not set or is empty
            // then allow it through
            if (!required($field) && strcmp($_POST[$field['field']],"") == 0 )
            {
                $_POST[$field['field']] = null;
            }
            else
            {
                // Multi select fields come back with an array in the post data
                // In the case of these we need to treat them differently.
                // We need to validate every value in the array instead of just
                // the post data
                if (is_array($_POST[$field['field']]))
                {
                    foreach ($_POST[$field['field']] as $value)
                    {
                        if (!preg_match($validation_types[$field['validation_type']],$value))
                        {
                            $field['error'] = $field['name'] . " is not valid";
                            $errorFlag = true;
                        }
                    }
                }
                else
                {
                    if (!preg_match($validation_types[$field['validation_type']],$_POST[$field['field']]))
                    {
                        $field['error'] = $field['name'] . " is not valid";
                        $errorFlag = true;
                    }
                }
            }
        }
    }

    // Since we are setting a flag to true for an error and we want the function to return false on error
    // we need to return NOT errorFlag
    return !$errorFlag;
}

function populateVariables()
{
    global $table, $pk_field, $lookup_key, $metaInfo, $view;

    // If a view has been specified we want to query the view not the table
    if ($view)
    {
        $row = getRecord($view,$pk_field,$lookup_key);
    }
    else
    {
        $row = getRecord($table,$pk_field,$lookup_key);
    }


    if (!$row)
    {
        cleanUp();
        die("ERROR: We couldn't find the primary key requested $query.<br /><br />");
    }
    // It is neccesary to force the field param to be a reference,
    // if you do not specifiy this you are actually working on a
    // copy of the array element, this way you are given a ref
    // to the real one instead
    foreach ($metaInfo as &$field) {
        // If it is a boolean field (checkbox) we need to translate
        if (strcmp($field['form_type'],"checkbox") == 0)
        {
            if (strcmp($row[$field['field']],"t") == 0 || $row[$field['field']] == 1) {
                $field['value'] = 1;
            }
            else {
                $field['value'] = 0;
            }
        }
        elseif (preg_match("/multiselect_lookup$/",$field['form_type'])) {
            $field['value'] = array();

            // Do a query on the mk_table with the current pk
            $query = "SELECT * FROM " . $field['optional_args']['mk_table'] . " WHERE " . $field['optional_args']['mk_main_pk'] . "=?";
            $params = array($lookup_key);

            if (!startMultiRowLookup($query, $params)) {
                $db_err = getLastError();
                cleanUp();
                var_dump($field);
                die("ERROR: Couldn't execute multikey query [$query]: DBERR: $db_err");
            }

            while ($mk_row = getNextRowAssoc()) {
                $field['value'][] = $mk_row[$field['optional_args']['mk_list_pk']];
            }
        }
        else {
            $field['value'] = $row[$field['field']];
        }
    }
}

/***********************************************************************/

function isMultiselect($field)
{
    if (strstr($field['form_type'],"multiselect"))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function hasExternalLink($field)
{
    if (array_key_exists("ext_link", $field['optional_args'])) {
        return true;
    }
    return false;
}

function hasParent($field)
{
    if (array_key_exists("parent", $field['optional_args'])) {
        return true;
    }
    return false;
}

function hasEditLink($field)
{
    if (array_key_exists("edit_link", $field["optional_args"]) && strcmp($field["optional_args"]["edit_link"],"true") == 0) {
        return true;
    }
    return false;
}

function listHide($field)
{
    if (array_key_exists("list_hide", $field["optional_args"]) && strcmp($field["optional_args"]["list_hide"],"hide") != 0) {
        return true;
    }
    return false;
}

function hiddenField($field)
{
    if (strcmp($field['form_type'],"hidden") == 0) {
        return true;
    }
    return false;
}

function hasDependency($field)
{
    if (array_key_exists('dependency', $field['optional_args'])) {
        return true;
    }
    return false;
}

function required($field)
{
    if ($field['required'] == 'false') {
        return false;
    }
    else if ($field['required'] == 'true') {
        return true;
    }
    else {
        die("Can't understand field required value");
    }
}

function viewOnly($field)
{
    if (isset($field['optional_args']['view_only']) &&
        $field['optional_args']['view_only'] === "true")
    {
        return true;
    }
    else
    {
        return false;
    }
}

function cleanValue($field)
{
    if (isset($field['value'])) {
        return $field['value'];
    }

    return "";
}

function printTitle()
{
    global $modes, $mode, $file;

    $pageType = "";

    foreach ($modes as $modeName => $modeId)
    {
        if ($modeId == $mode)
        {
            if ($mode == $modes['new'])
            {
                $pageType = "Add";
            }
            else
            {
                $pageType = $modeName;
            }
        }
    }

    print "<h1 class='pageTitle'>" . ucwords($pageType) . " " . ucwords(str_replace("_"," ",$file)) . "</h1>";
}

function confirmDelete()
{
    if (isset($_REQUEST['confirm']))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function cleanTextForInput($text)
{
    return htmlspecialchars(rawurldecode($text));
}

function cleanUrlText($text)
{
    return nl2br(cleanTextForInput($text));
}
?>
