<?php

require_once("src/Formlib/Fields/Field.php");
require_once("src/Formlib/Fields/HiddenField.php");
require_once("src/Formlib/Fields/CheckboxField.php");
require_once("src/Formlib/Fields/TextAreaField.php");
require_once("src/Formlib/Fields/DropdownField.php");
require_once("src/Formlib/Fields/DropdownLookupField.php");
require_once("src/Formlib/Fields/ListField.php");
require_once("src/Formlib/Fields/ListLookupField.php");
require_once("src/Formlib/Fields/ListMultiselectField.php");
require_once("src/Formlib/Fields/ListMultiselectLookupField.php");
require_once("src/Formlib/Fields/RadioField.php");
require_once("src/Formlib/Fields/RadioLookupField.php");
require_once("src/Formlib/Fields/FieldException.php");

class FieldFactory
{
	protected $_lookupTableFactory = null;

	public function __construct($lookupTableFactory)
	{
		if (!$lookupTableFactory instanceof LookupTableFactory)
		{
			throw new FieldException("FieldFactory not given a LookupTable");
		}
		$this->_lookupTableFactory = $lookupTableFactory;
	}

	public function createField($fieldInfo)
	{
		if (!is_array($fieldInfo))
		{
			throw new FieldException("fieldInfo must be an array");
		}

		$field = null;
		$class = null;

		switch ($fieldInfo['type'])
		{
			case "text":
				$class = "Field";
				break;
			case 'textarea':
				$class = "TextAreaField";
				break;
			case 'hidden':
				$class = "HiddenField";
				break;
			case 'checkbox':
				$class = "CheckboxField";
				break;
			case 'dropdown':
				$class = "DropdownField";
				break;
			case 'dropdown_lookup':
				$class = "DropdownLookupField";
				break;
			case 'list':
				$class = "ListField";
				break;
			case 'list_lookup':
				$class = "ListLookupField";
				break;
			case 'list_multiselect':
				$class = "ListMultiselectField";
				break;
			case 'list_multiselect_lookup':
				$class = "ListMultiselectLookupField";
				break;
			case 'radio':
				$class = "RadioField";
				break;
			case 'radio_lookup':
				$class = "RadioLookupField";
				break;
			default:
				throw new FieldException("No field type matches \"" . $fieldInfo['type'] . "\"");
		}

		return new $class(
			$fieldInfo['field'],
			$fieldInfo['name'],
			$fieldInfo['type'],
			$fieldInfo['validator'],
			$fieldInfo['required'],
			$fieldInfo['optionalArgs'],
			$fieldInfo['database_type'],
			$this->_lookupTableFactory
		);
	}
}

