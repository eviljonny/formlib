<?php

require_once("src/Formlib/Fields/ListTypeField.php");

class TextAreaField extends Field
{
	public function renderFormElement()
	{
		$field = "";

		$field .= $this->_renderPreElement();

		$readOnly = "";
		if (!$this->_editable)
		{
			$readOnly = "readonly='readonly' ";
		}

		$field .=
			"<textarea " .
				"name='" . $this->_name . "' " .
				$readOnly . ">" .
					$this->_value .
			"</textarea>"
		;

		$field .= $this->_renderPostElement();

		return $field;
	}
}
