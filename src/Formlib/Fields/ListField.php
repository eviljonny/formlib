<?php

require_once("src/Formlib/Fields/ListTypeField.php");

class ListField extends ListTypeField
{
	protected $_multiselect = false;
	protected $_listSize = 8;

	public function renderFormElement()
	{
		$field = "";

		$field .= $this->_renderPreElement();

		$readOnly = "";
		if ($this->_editable)
		{
			$readOnly = "readonly='readonly' ";
		}

		$multi = "";
		$name = "name='" . $this->_name . "' ";
		if ($this->_multiselect)
		{
			$multi = "multiple='multiple' ";
			$name = "name='" . $this->_name . "[]' ";
		}

		$field .= "<select "
				. $name
				. $readonly
				. $multi
				. "size='" . $this->_listSize . "'"
				. ">\n";

		foreach ($this->_listData as $key=>$val)
		{
			$selected = "";
			if ($this->_value == $val)
			{
				$selected = "selected='selected'";
			}

			$field .= "<option value='" . $key . "' $selected>" .
				$val . "</option>\n";

		}
		$field .= "</select>";

		$field .= $this->_renderPostElement();

		return $field;
	}
}
