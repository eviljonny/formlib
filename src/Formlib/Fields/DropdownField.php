<?php

require_once("src/Formlib/Fields/ListTypeField.php");

class DropdownField extends ListTypeField
{
	public function renderFormElement()
	{
		$field = "";

		$field .= $this->_renderPreElement();

		$readOnly = "";
		if ($this->_editable)
		{
			$readOnly = "readonly='readonly' ";
		}


		$field .= "<select "
				. "name='" . $this->_name . "' "
				. $readonly
				. ">\n";

		foreach ($this->_listData as $key=>$val)
		{
			$selected = "";
			if ($this->_value == $val)
			{
				$selected = "selected='selected'";
			}

			$field .= "<option value='" . $key . "' $selected>" .
				$val . "</option>\n";

		}
		$field .= "</select>";

		$field .= $this->_renderPostElement();

		return $field;
	}
}
