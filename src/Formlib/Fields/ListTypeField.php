<?php

require_once("src/Formlib/Fields/Field.php");
require_once("src/Formlib/Fields/FieldException.php");

abstract class ListTypeField extends Field
{
	protected $_listData = null;
	protected $_lookupType = false;

	public function __construct(
		$name, $displayName, $type, $validator,
		$required=false, $optionalArguments,
		$databaseType, $lookupTableFactory
	)
	{
		parent::__construct(
			$name, $displayName, $type, $validator,
			$required=false, $optionalArguments,
			$databaseType, $lookupTableFactory
		);

		$dropdownData = array();

		# By this point the only things left in the
		# optional args array should be the data
		$this->_loadListData();
	}

	protected function _loadListData()
	{
		$data = null;

		if ($this->_lookupType === true)
		{
			if (!$this->_lookupTable instanceof LookupTable)
			{
				throw new FieldException("A Lookup Field requires a lookup table");
			}

			$data = $this->_lookupTable->getAllValues();
		}
		else
		{
			$data = $this->_optionalArgs;
			asort($data);
		}

		foreach ($data as $key => $val)
		{
			$this->_listData[$key] = $val;
		}
	}
}
