<?php

require_once("src/Formlib/Fields/FieldException.php");

class Field
{
	public static $TYPES = array(
		'hidden',
		'text',
		'dropdown',
		'dropdown_lookup',
		'list',
		'list_lookup',
		'list_multiselect',
		'list_multiselect_lookup',
		'checkbox',
		'radio',
		'radio_lookup',
		'textarea'
	);
	protected $_name = null;
	protected $_displayName = null;
	protected $_validator = null;
	protected $_type = null;
	protected $_databaseType = null;
	protected $_parent = null;
	protected $_required = false;
	protected $_hidden = false;
	protected $_editable = true;
	protected $_optionalArgs = null;
	protected $_lookupTableFactory = null;
	protected $_value = "";
	protected $_namePattern = '/^[A-Za-z0-9_]+$/';
	protected $_displayNamePattern = '/^[A-Za-z0-9_ ]+$/';

	public function __construct(
		$name, $displayName, $type, $validator,
		$required=false, $optionalArguments,
		$databaseType, $lookupTableFactory
	)
	{
		if (!preg_match($this->_namePattern, $name))
		{
			throw new FieldException("Field name $name doesn't match $this->_namePattern");
		}
		$this->_name = $name;

		if (!preg_match($this->_displayNamePattern, $displayName))
		{
			throw new FieldException("Field display name $displayName doesn't match $this->_displayNamePattern");
		}
		$this->_displayName = $displayName;

		if (!in_array($type, self::$TYPES))
		{
			throw new FieldException("Type \"" . $type . "\" is not valid");
		}
		$this->_type = $type;

		if (! $validator instanceof Validator)
		{
			throw new FieldException("You must give a Validtor class for the validator of a field");
		}
		$this->_validator = $validator;
		$this->_required = $required;

		if (!is_array($optionalArguments))
		{
			throw new FieldException("Optional Args needs to be an array");
		}
		$this->_optionalArgs = $optionalArguments;
		$this->_databaseType = $databaseType;

		if (!$lookupTableFactory instanceof LookupTableFactory)
		{
			throw new FieldException("Field has not been provided with a LookupTableFactory");
		}
		$this->_lookupTableFactory = $lookupTableFactory;

		$this->_parseOptionalArguments();
	}

	protected function _parseOptionalArguments()
	{
		if (array_key_exists("table", $this->_optionalArgs))
		{
			$this->_loadLookupTable(
				$this->_optionalArgs['table']
			);
		}
	}

	protected function _loadLookupTable($table)
	{
		if (!array_key_exists("key", $this->_optionalArgs))
		{
			throw new FieldException(
				"lookup table " . $table .
				" specified for field " . $this->_name .
				" but no primary key specified"
			);
		}
		if (!array_key_exists("val", $this->_optionalArgs))
		{
			throw new FieldException(
				"lookup table " . $table .
				" specified for field " . $this->_name .
				" but no value column specified"
			);
		}

		$this->_lookupTable = $this->_lookupTableFactory->getLookupTable(
			$table,
			$this->_optionalArgs['key'],
			$this->_optionalArgs['val']
		);
	}

	public function __toString()
	{
		return $this->_name;
	}

	public static function typeExists($type)
	{
		if (in_array($type, self::$TYPES))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function renderLabel()
	{
		$label = "";

		if (!$this->_hidden)
		{
			$label .= FormlibConfig::$formatting['prelabel'] . "\n";
			$label .= $this->_displayName . "\n";
			$label .= FormlibConfig::$formatting['postlabel'] . "\n";
		}

		return $label;
	}

	public function renderFormElement()
	{
		$field = "";

		$field .= $this->_renderPreElement();

		$readOnly = "";
		if ($this->_editable)
		{
			$readOnly = "readonly='readonly' ";
		}

		$field .= "<input type='" . $this->_type . "' "
				. "name='" . $this->_name . "' "
				. "value='" . $this->_value . "' "
				. $readonly
				. " />";

		$field .= $this->_renderPostElement();

		return $field;
	}

	public function renderError()
	{
		if ($this->_hidden)
		{
			return "";
		}
		else
		{
			$error = FormlibConfig::$formatting['preerror'];

			if ($this->_error)
			{
				$error .= $this->_error;
			}
			else
			{
				$error .= "&nbsp;";
			}
			$error .= FormlibConfig::$formatting['posterror'] . "\n";

			return $error;
		}
	}

	protected function _renderPreElement()
	{
		if ($this->_hidden)
		{
			return "";
		}
		else
		{
			return FormlibConfig::$formatting['prefield'] . "\n";
		}
	}

	protected function _renderPostElement()
	{
		if ($this->_hidden)
		{
			return "";
		}
		else
		{
			return "\n" . FormlibConfig::$formatting['postfield'] . "\n";
		}
	}

	public function setValue($value)
	{
		$this->_value = $value;
	}

	public function renderTableHeader()
	{
		return $this->_displayName;
	}

	public function renderTableValue()
	{
		return $this->_value;
	}
}
