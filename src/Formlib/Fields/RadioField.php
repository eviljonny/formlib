<?php

require_once("src/Formlib/Fields/ListTypeField.php");

class RadioField extends ListTypeField
{
	public function renderFormElement()
	{
		$field = "";

		$field .= $this->_renderPreElement();

		$readOnly = "";
		if (!$this->_editable)
		{
			$readOnly = "readonly='readonly' ";
		}

		foreach ($this->_listData as $key=>$val)
		{
			$selected = "";
			if ($this->_value == $val)
			{
				$selected = "selected='selected'";
			}

			$field .=
				"<input type='radio' " .
					"name='" . $this->_name . "' " .
					"value='" . $key . "' " .
					$selected . " " .
					$readOnly . " " .
				">" .
					$val .
				"</input>\n";
		}

		$field .= $this->_renderPostElement();

		return $field;
	}
}
