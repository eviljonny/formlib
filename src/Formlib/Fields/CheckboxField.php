<?php

require_once("src/Formlib/Fields/Field.php");

class CheckboxField extends Field
{
	public function renderFormElement()
	{
		$field = "";

		$field .= $this->_renderPreElement();

		if ($this->_value === true)
		{
			$selected = "selected='selected'";
		}

		$field .=
			"<input type='checkbox' " .
				"name='" . $this->_name . "' " .
				"value='" . $this->_name . "' " .
				$selected . " " .
			"/>";

		$field .= $this->_renderPostElement();

		return $field;
	}
}
