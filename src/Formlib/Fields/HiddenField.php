<?php

require_once("src/Formlib/Fields/Field.php");

class HiddenField extends Field
{
	protected $_hidden = true;

	public function renderFormElement()
	{
		$field =
			"<input type='hidden' " .
				"name='" . $this->_name . "' " .
				"value='" . $this->_value . "' " .
			"/>\n";

		return $field;
	}
}
