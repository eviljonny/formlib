<?php

require_once("src/Formlib/Validators/ValidationException.php");

class Validator
{
	protected $_regex;
	protected $_name;

	function __construct($name, $regex)
	{
		$this->_name = $name;
		$this->_regex = $regex;
	}

	public function validate($string)
	{
		$result = preg_match($this->_regex, $string);
		if ($result === 1) return true;
		elseif ($result === 0) return false;
		else throw Exception("preg_match error in Validator");
	}

	public function getName()
	{
		return $this->_name;
	}
}

?>
