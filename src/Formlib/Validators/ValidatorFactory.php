<?php

require_once("src/Formlib/Validators/MissingValidatorException.php");
require_once("src/Formlib/Validators/Validator.php");
require_once("src/Formlib/Config/ValidationFileParser.php");

class ValidatorFactory
{
	protected $_validators = null;

	public function __construct($validationFile)
	{
		$this->_validators = array();

		$parser = new ValidationFileParser($validationFile);

		$parser->parseValidationFile(
			array(
				$this,
				'addValidator'
			)
		);
	}

	public function addValidator($validator)
	{
		$this->_validators[$validator->getName()] = $validator;
	}

	public function getValidator($name)
	{
		if (!array_key_exists($name, $this->_validators))
		{
			throw new MissingValidatorException("No validator named " . $name);
		}

		return $this->_validators[$name];
	}

	public function exists($name)
	{
		if (array_key_exists($name, $this->_validators))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

