<?php

require_once("src/Formlib/QuickDB/QuickDBException.php");

class QuickDb
{
	protected $_quickdb_multirow_lookup = null;
	protected $_dbh = null;

	public function __construct($databaseDetails)
	{
		if (!is_array($databaseDetails))
		{
			throw new QuickDBException("No database details provided");
		}

		$this->_dbh = pg_connect(
			"dbname=" . $databaseDetails['database'] . " " .
			"host=" . $databaseDetails['hostname'] . " " .
			"user=" . $databaseDetails['username'] . " " .
			"password=" . $databaseDetails['password']
		);

		if (!$this->_dbh)
		{
			throw new QuickDBException(
				"Couldn't connect to configured database"
			);
		}
	}

	public function getDatabaseHandle()
	{
		return $this->_dbh;
	}

	public function disconnectFromDatabase()
	{
		pg_close($this->_dbh);
	}

	public function getRecord($table, $pk_field, $lookup_key)
	{
		// If the primary key type is non-numeric we need to quote it
		if (is_numeric($lookup_key))
		{
			$query = "SELECT * FROM $table WHERE $pk_field=$lookup_key";
		}
		else
		{
			$query = "SELECT * FROM $table WHERE $pk_field='$lookup_key'";
		}

		// Perform the query to get the row to be edited from the database
		// If the query fails or returns more or less than one row die
		// otherwise get the associative array of the columns
		$result = pg_query($this->_dbh, $query);
		if (!$result)
		{
			$this->cleanUp();
			throw new QuickDBException("ERROR: Couldn't perform query $query");
		}

		$num_rows = pg_num_rows($result);
		if ($num_rows != 1)
		{
			return false;
		}

		return pg_fetch_assoc($result);
	}

	public function startMultiRowLookup($query)
	{
		$this->_quickdb_multirow_lookup = pg_query($this->_dbh, $query);

		if ($this->_quickdb_multirow_lookup)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getLastError()
	{
		return pg_last_error($this->_dbh);
	}

	public function getNextRowAssoc()
	{
		return pg_fetch_assoc($this->_quickdb_multirow_lookup);
	}

	public function getNextRow()
	{
		return pg_fetch_row($this->_quickdb_multirow_lookup);
	}

	public function numRows()
	{
		return pg_num_rows($this->_quickdb_multirow_lookup);
	}

	public function escape($string)
	{
		// Need a more modern version of PHP (>= 5.4.4) to use
		// pg_escape_literal
		// return pg_escape_literal($this->_dbh, $string);
		return "'" . pg_escape_string($this->_dbh, $string) . "'";
	}
}
?>
