<?php

require_once("src/Formlib/QuickDB/QuickDB.php");
require_once("src/Formlib/QuickDB/LookupTableException.php");

class LookupTable
{
	protected $_table = null;
	protected $_pk = null;
	protected $_lookupField = null;
	protected $_values = null;
	protected $_quickdb = null;
	protected $_dbh = null;
	protected $_order = null;

	public function __construct($quickdb, $table, $pk, $lookupField)
	{
		if (!$quickdb instanceof QuickDB)
		{
			throw new LookupTableException("QuickDB Instance not given to LookupTable");
		}

		$this->_values = array();
		$this->_order = array();
		$this->_quickdb = $quickdb;
		$this->_dbh = $this->_quickdb->getDatabaseHandle();
		$this->_table = $table;
		$this->_pk = $pk;
		$this->_lookupField = $lookupField;

		$this->_loadLookupTable();
	}

	protected function _loadLookupTable()
	{
		$query = "
		    SELECT " . $this->_pk . "," . $this->_lookupField . "
		      FROM " . $this->_table . "
		  ORDER BY " . $this->_lookupField . "
		";

		$this->_quickdb->startMultiRowLookup($query);

		while ($row = $this->_quickdb->getNextRow())
		{
			list($pk, $value) = $row;

			$this->_order[] = $pk;
			$this->_values[$pk] = $value;
		}
	}

	public function getValue($key)
	{
		if (!array_key_exists($key, $this->_values))
		{
			throw new LookupTableException(
				"Table " . $this->_table .
				" has no key " . $key
			);
		}

		return $this->_values[$key];
	}

	public function getAllValues()
	{
		return $this->_values;
	}
}
?>
