<?php

require_once("src/Formlib/QuickDB/LookupTable.php");
require_once("src/Formlib/QuickDB/LookupTableException.php");

class LookupTableFactory
{
	protected $_lookupTables = null;
	protected $_quickdb = null;

	public function __construct($quickdb)
	{
		$this->_lookupTables = array();
		$this->_quickdb = $quickdb;
	}

	public function getLookupTable($table, $pk, $lookupField)
	{
		$this->preloadLookupTable($table, $pk, $lookupField);

		return $this->_lookupTables[$table][$pk][$lookupField];
	}

	public function preloadLookupTable($table, $pk, $lookupField)
	{
		if (!isset($this->_lookupTables[$table][$pk][$lookupField]))
		{
			$this->_newLookupTable($table, $pk, $lookupField);
		}
	}

	protected function _newLookupTable($table, $pk, $lookupField)
	{
		$this->_lookupTables[$table][$pk][$lookupField] = new LookupTable(
			$this->_quickdb,
			$table,
			$pk,
			$lookupField
		);
	}
}
