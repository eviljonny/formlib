<?php

require_once("src/Formlib/QuickDB/QuickDB.php");
require_once("src/Formlib/QuickDB/DBInspectorException.php");

class DBInspector
{
	public static $COLUMN_TYPES = array(
		"char" => "string",
		"character" => "string",
		"character varying" => "string",
		"varchar" => "string",
		"integer" => "numeric",
		"int" => "numeric",
		"int4" => "numeric",
		"numeric" => "numeric",
		"date" => "string",
		"text" => "string",
		"cidr" => "string",
		"bigint" => "numeric",
		"int8" => "numeric",
		"bigserial" => "numeric",
		"serial8" => "numeric",
		"boolean" => "boolean",
		"bool" => "boolean",
		"bytea" => "string",
		"double precision" => "numeric",
		"float8" => "numeric",
		"inet" => "string",
		"macaddr" => "string",
		"real" => "numeric",
		"float4" => "numeric",
		"int2" => "numeric",
		"smallint" => "numeric",
		"serial" => "numeric",
		"serial4" => "numeric",
		"time" => "string",
		"timestamp" => "string",
		"timestamp without time zone" => "string",
		"tsquery" => "string",
		"tsvector" => "string",
		"xml" => "string",
	);
	protected $_tables = null;
	protected $_quickDB = null;
	protected $_dbh = null;

	public function __construct($quickDB)
	{
		if (!$quickDB instanceof QuickDB)
		{
			throw new DBInspectorException("QuickDB Instance not given to DBInspector");
		}

		$this->_tables = array();
		$this->_quickDB = $quickDB;
		$this->_dbh = $this->_quickDB->getDatabaseHandle();
	}

	public function getColumnType($table, $column)
	{
		if (!array_key_exists($table, $this->_tables))
		{
			$this->_loadTable($table);
		}

		if (array_key_exists($column, $this->_tables[$table]))
		{
			return $this->_tables[$table][$column];
		}
		else
		{
			throw new DBInspectorException(
				"Column \"" . $column . "\" " .
				"does not exist in table \"" . $table . "\""
			);
		}
	}

	protected function _loadTable($table)
	{
		if (!array_key_exists($table, $this->_tables))
		{
			if (!$this->_quickDB->startMultiRowLookup(
					"SELECT column_name,
					        data_type
					   FROM information_schema.columns
					  WHERE table_name=" . $this->_quickDB->escape($table) . "
					"
				)
			)
			{
				throw new DBInspectorException(
					"Couldn't execute query to get column types " .
					"error was " . $this->_quickDB->getLastError()
				);
			}

			while ($row = $this->_quickDB->getNextRowAssoc())
			{
				if (array_key_exists($row['data_type'], self::$COLUMN_TYPES))
				{
					$this->_tables[$table][$row['column_name']]
						= self::$COLUMN_TYPES[$row['data_type']];
				}
				else
				{
					throw new DBInspectorException(
						"Could not understand datatype of column \"" .
						$row['column_name'] . "\" with type \"" .
						$row['data_type'] . "\""
					);
				}
			}
		}
	}
}
?>
