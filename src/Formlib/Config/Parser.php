<?php

abstract class Parser
{
	protected $_file_handle = null;
	protected $_valid_column_regex = '/^[A-Za-z0-9_-]+$/';
	protected $_exceptionType;
	protected $_description = null;

	public function __construct($description)
	{
		$this->_exceptionType = $this->_getExceptionType();
		$this->_description = $description;
	}

	protected function _getExceptionType()
	{
		$class = get_class($this);

		# Replace Parser from the classname and replace with Exception
		$exceptionName = substr($class, 0, -6) . "Exception";

		return $exceptionName;
	}

	protected function _loadFile($file)
	{
		$class = get_class();

		if (!file_exists($file))
		{
			throw new $this->_exceptionType("Can't find " . $this->_description . " " . $file);
		}

		$this->_file_handle = fopen($file, "r");

		if ($this->_file_handle === false)
		{
			throw new $this->_exceptionType("Couldn't open " . $this->_description . " " . $file);
		}

		return true;
	}

	protected function _getNextLine()
	{
		$line = null;

		do
		{
			$line = fgets($this->_file_handle, 1024);
			if ($line === false)
			{
				fclose($this->_file_handle);
				return false;
			}

			$line = trim($line);
			$line = $this->_stripComment($line);
		} while ($line === "");

		return $line;
	}

	protected function _stripComment($string)
	{
		$hashPosition = strpos($string, "#");

		if ($hashPosition === false)
		{
			return $string;
		}

		$string = substr($string, 0, $hashPosition);

		return $string;
	}

	protected function _isValidColumnName($column)
	{
		$result = preg_match($this->_valid_column_regex, $column);

		if ($result === 1)
		{
			return true;
		}
		elseif ($result === 0)
		{
			return false;
		}
		else
		{
			debug_print_backtrace();
			throw new $this->_exceptionType("Couldn't validate column name " . $column . " for " . $this->_description);
		}
	}
}

?>
