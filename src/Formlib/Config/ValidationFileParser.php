<?php

require_once("src/Formlib/Config/ValidationFileException.php");
require_once("src/Formlib/Config/Parser.php");

class ValidationFileParser extends Parser
{
	protected $_validationFile = null;
	protected $_file_handle = null;
	protected $_valid_column_regex = '/^[A-Za-z0-9_-]+$/';
	protected $_maxValidationDefinitions = 500;

	public function __construct($validationFile)
	{
		parent::__construct("validation file");

		if (!isset($validationFile))
		{
			throw new ValidationFileException("No validation file provided");
		}

		if (!preg_match('/^[\/A-Za-z0-9_-]+\.conf$/', $validationFile))
		{
			throw new ValidationFileException("Validation file name not valid");
		}

		$this->_validationFile = $validationFile;
	}

	public function parseValidationFile($callbackFunction)
	{
		if (!is_callable($callbackFunction))
		{
			throw new ValidationFileException("Invalid callback function");
		}

		$this->_loadValidationFile();

		$count = 0;
		while ($validator = $this->_getNextValidator())
		{
			if (++$count >= $this->_maxValidationDefinitions)
			{
				throw new ValidationFileException(
					$this->_maxValidationDefinitions . " or more lines in "
					. " validation file, sounds broken to me, exiting"
				);
			}

			call_user_func($callbackFunction, $validator);
		}
	}

	protected function _getNextValidator()
	{
		$line = $this->_getNextLine();

		if ($line)
		{
			list ($name, $regex) = explode(":", $line, 2);

			if (!isset($name) || !isset($regex))
			{
				throw new ValidationFileException("Name and regex must be set");
			}

			if (!$this->_isValidColumnName($name))
			{
				throw new ValidationFileException("Invalid name for validator");
			}

			return new Validator($name, $regex);
		}
		else
		{
			return false;
		}
	}

	protected function _loadValidationFile()
	{
		return $this->_loadFile($this->_validationFile);
	}
}

?>
