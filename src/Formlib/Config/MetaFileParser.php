<?php

require_once("src/Formlib/Config/MetaFileException.php");
require_once("src/Formlib/Fields/Field.php");
require_once("src/Formlib/Config/Parser.php");
require_once("src/Formlib/QuickDB/DBInspector.php");

class MetaFileParser extends Parser
{
	protected $_basedir = null;
	protected $_page = null;
	protected $_file_handle = null;
	protected $_valid_column_regex = '/^[A-Za-z0-9_ -]+$/';
	protected $_table = null;
	protected $_primary_key = null;
	protected $_list_order = null;
	protected $_view = null;
	protected $_maxFieldCount = 500;
	protected $_maxFieldsInTableLine = 4;
	protected $_maxFieldsInFieldLine = 4;
	protected $_validatorFactory = null;
	protected $_fieldFactory = null;
	protected $_dbInspector = null;

	public function __construct($basedir, $page, $validatorFactory, $fieldFactory, $dbInspector)
	{
		parent::__construct("meta file");

		if (!isset($page))
		{
			throw new MetaFileException("No page provided");
		}

		if (!preg_match('/^[A-Za-z0-9_-]+$/', $page))
		{
			throw new MetaFileException("Page name not valid");
		}

		if (!$validatorFactory instanceof ValidatorFactory)
		{
			throw new MetaFileException("MetaFileParser was not handed a ValidatorFactory");
		}

		if (!$fieldFactory instanceof FieldFactory)
		{
			throw new MetaFileException("MetaFileParser was not handed a FieldFactory");
		}

		if (!$dbInspector instanceof DBInspector)
		{
			throw new MetaFileException("MetaFileParser was not handed a DBInspector");
		}

		$this->_basedir = $basedir;
		$this->_page = $page;
		$this->_validatorFactory = $validatorFactory;
		$this->_fieldFactory = $fieldFactory;
		$this->_dbInspector = $dbInspector;
	}

	public function parseMetaFile($callbackFunction)
	{
		if (!is_callable($callbackFunction))
		{
			throw new MetaFileException("Invalid callback function");
		}

		$this->_loadMetaFile();
		$this->_parseTableInformation();

		$fieldCount = 0;
		while ($field = $this->_getNextField())
		{
			if (++$count >= $this->_maxFieldCount)
			{
				throw new MetaFileException("500 or more lines in metafile for page " . $this->_page . ", sounds broken to me, exiting");
			}
			call_user_func($callbackFunction, $field);
		}
	}

	protected function _getNextField()
	{
		$tableLine = $this->_getNextLine();

		if ($tableLine)
		{
			# FIXME
			# What I actually need to do is parse the info
			# and then create a field from it
			//public function __construct($name, $displayName, $validator, $required=false)
	//public function __construct($name, $displayName, $type, $validator, $required=false, $optionalArguments)
			$fieldInfo = $this->_parseFieldInfo($tableLine);
			$field = $this->_fieldFactory->createField($fieldInfo);
			return $field;
		}
		else
		{
			return false;
		}
	}

	protected function _parseOptionalFields($optionalString)
	{
		$optionalArgs = array();

		if ($optionalString == null || strlen($optionalString) == 0)
		{
			return $optionalArgs;
		}

		$pairs = explode(";",$optionalString);

		foreach($pairs as $pair)
		{
			list ($argName, $argValue) = explode("=", $pair, 2);
			if ($argName && strlen($argName) > 0 && $argValue && strlen($argValue) > 0)
			{
				$optionalArgs[$argName] = $argValue;
				# FIXME - we have to move the dependency caluclation to somewhere else
				// If this arg says this field is dependant on another we should put the name
				// of the other into the dependancy array
				// if (strcmp($arg[0],"parent") == 0)
				//{
				//	$dependencies[] = $arg[1];
				//}
			}
			else
			{
				throw MetaFileException(
					"Optional argument \"" . $argName
					. "\" invalid in metafile for page \"" . $this->_page . "\""
				);
			}
		}

		return $optionalArgs;
	}

	protected function _parseFieldInfo($text)
	{
		$array = explode(":",$text,6);
		if (count($array)!=6)
		{
			throw new MetaFileException("Metafile for page \"" . $this->_page
				. "\" does not have the required number of arguments on "
				. "the following line: \"" . $text . "\"");
		}

		$fieldInfo = array();

		$fieldInfo['field'] = $array[0];
		$fieldInfo['name'] = $array[1];
		$fieldInfo['type'] = $array[2];
		$fieldInfo['validator'] = $array[3];
		$fieldInfo['required'] = $array[4];
		$fieldInfo['optionalArgs'] = $this->_parseOptionalFields($array[5]);
		$fieldInfo['database_type'] = null;

		if (!$this->_isValidColumnName($fieldInfo['field']))
		{
			throw new MetaFileException(
				"Field column name \"" . $fieldInfo['field']
				. " invalid in metafile for page \"" . $this->_page
				. "\" on line \"" . $text . "\""
			);
		}

		if (!$this->_isValidColumnName($fieldInfo['name']))
		{
			throw new MetaFileException(
				"Field display name \"" . $fieldInfo['name']
				. "\" invalid in metafile for page \"" . $this->_page
				. "\" on line \"" . $text . "\""
			);
		}

		if (!$this->_isValidColumnName($fieldInfo['type'])
			&& !FieldFactory::typeExists($fieldInfo['type'])
		)
		{
			throw new MetaFileException(
				"Form type \"" . $fieldInfo['type']
				. "\" invalid in metafile for page \"" . $this->_page
				. "\" on line \"" . $text . "\""
			);
		}
		$fieldInfo['database_type'] =
			$this->_dbInspector->getColumnType(
				$this->_table,
				$fieldInfo['field']
			);

		if (!$this->_isValidColumnName($fieldInfo['validator'])
			&& !$this->_validatorFactory->exists($fieldInfo['validator'])
		)
		{
			throw new MetaFileException(
				"Validation type \"" . $fieldInfo['validator']
				. "\" invalid in metafile for page \"" . $this->_page
				. "\" on line \"" . $text . "\""
			);
		}
		$fieldInfo['validator'] = $this->_validatorFactory->getValidator($fieldInfo['validator']);

		if ($fieldInfo['required'] !== "true" && $fieldInfo['required'] !== "false")
		{
			throw new MetaFileException(
				"Required definition \"" . $fieldInfo['required']
				. "\" invalid in metafile for page \"" . $this->_page
				. "\" on line \"" . $text . "\""
			);
		}

		return $fieldInfo;
	}

	protected function _parseTableInformation()
	{
		$tableLine = $this->_getNextLine();

		if ($tableLine === false)
		{
			throw new MetaFileException("Couldn't find a table description line in metafile for page " . $this->_page);
		}

		// Line is TABLE=whatever;PK=<primary_key_column_name>;LIST_ORDER=<columns_to_sort_on>
		list ($tableOption, $pkOption, $listOption, $viewOption) = explode(";", $tableLine, $this->_maxFieldsInTableLine);
		if (!isset($tableOption) || !isset($pkOption))
		{
			throw new MetaFileException("First line of the meta file for page '" . $this->_page . "' did not contain TABLE and PK declarations");
		}

		list ($tableDeclaration, $tableName) = explode("=", $tableOption);
		list ($pkDeclaration, $pk) = explode("=", $pkOption);

		$listDeclation = null;
		$listOrder = null;
		if ($listOption === "")
		{
			$listOption = null;
		}

		$viewDeclaration = null;
		$view = null;
		if ($viewOption === "")
		{
			$viewOption = null;
		}

		if (isset($listOption))
		{
			list ($listDeclaration, $listOrder) = explode("=", $listOption);
		}

		if (isset($viewOption))
		{
			list ($viewDeclaration, $view) = explode("=", $viewOption);
		}

		if ($tableDeclaration !== "TABLE")
		{
			throw new MetaFileException("TABLE declaration for " . $this->_page . " not understood");
		}
		if ($pkDeclaration !== "PK")
		{
			throw new MetaFileException("Primary Key declaration for " . $this->_page . " not understood");
		}

		if (isset($listOption) && $listDeclaration !== "LIST_ORDER")
		{
			throw new MetaFileException("List Order declaration for " . $this->_page . " not understood");
		}

		if (isset($viewOption) && $viewDeclaration !== "VIEW")
		{
			throw new MetaFileException("View order declaration for " . $this->_page . " not understood");
		}

		if (!$this->_isValidColumnName($tableName))
		{
			throw new MetaFileException("Table name is invalid for page " . $this->_page);
		}

		if (!$this->_isValidColumnName($pk))
		{
			throw new MetaFileException("Primary Key name is invalid for page " . $this->_page);
		}

		if (isset($listOption))
		{
			$columns = explode(",", $listOrder);
			foreach ($columns as $column)
			{
				if (!$this->_isValidColumnName($column))
				{
					throw new MetaFileException("Column " . $column . " in " . $this->_page . " as part of list order is invalid");
				}
			}
		}
		if (isset($viewOption) && !$this->_isValidColumnName($viewOption))
		{
			throw new MetaFileException("View name is invalid for page " . $this->_page);
		}

		$this->_table = $tableName;
		$this->_primary_key = $pk;
		$this->_list_order = $listOrder;
		$this->_view = $view;
	}

	protected function _loadMetaFile()
	{
		$filePath = $this->_basedir . "/metafiles/" . $this->_page . ".meta";

		return $this->_loadFile($filePath, "metafile");
	}
}

?>
