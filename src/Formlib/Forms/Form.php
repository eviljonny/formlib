<?php

require_once("src/Formlib/Forms/FormException.php");
require_once("src/Formlib/Config/MetaFileParser.php");
require_once("src/Formlib/Validators/ValidatorFactory.php");
require_once("src/Formlib/Fields/FieldFactory.php");

class Form
{
	protected $_fields = array();
	protected $_mode = null;
	protected $_metaFileParser = null;
	protected $_basedir = null;
	protected $_page = null;
	protected $_validatorFactory = null;
	protected $_fieldFactory = null;
	protected $_dbInspector = null;
	protected $_lookupTableFactory = null;
	protected $_scriptName = null;

	public function __construct($scriptName, $basedir, $page, $mode, $dbInspector, $lookupTableFactory)
	{
		if (!isset($page))
		{
			throw new FormException("No page provided");
		}

		if (!preg_match("/^[A-Za-z0-9_-]+$/", $page))
		{
			throw new FormException("Page name not valid");
		}

		if (!$dbInspector instanceof DBInspector)
		{
			throw new FormException("Form was not provided with a DBInspector");
		}

		if (!$lookupTableFactory instanceof LookupTableFactory)
		{
			throw new FormException("Form was not provided with a LookupTableFactory");
		}

		$this->_scriptName = $scriptName;
		$this->_basedir = $basedir;
		$this->_page = $page;
		$this->_mode = $mode;
		$this->_validatorFactory = new ValidatorFactory($basedir . "/config/validation_types.conf");
		$this->_lookupTableFactory = $lookupTableFactory;
		$this->_fieldFactory = new FieldFactory($this->_lookupTableFactory);
		$this->_dbInspector = $dbInspector;

		$this->_metaFileParser = new MetaFileParser(
			$this->_basedir,
			$this->_page,
			$this->_validatorFactory,
			$this->_fieldFactory,
			$this->_dbInspector
		);

		$this->_metaFileParser->parseMetaFile(
			array(
				$this,
				'addField'
			)
		);
	}

	protected function _renderBlankLine()
	{
		return "\n";
	}

	protected function _renderFormHeader()
	{
		$header = 	"<form method='post' " .
						"action='" . $this->_scriptName .
					"'>\n";

		$header .= $this->_renderPageElement() . "\n";

		$header .= $this->_renderModeElement() . "\n";

		$header .= FormlibConfig::$formatting['preform'] . "\n";

		return $header;
	}

	protected function _renderFormFooter()
	{
		$footer = FormlibConfig::$formatting['postform'] . "\n";
		$footer .= "</form>\n";

		return $footer;
	}

	# FIXME - other form types (edit, search and delete)
	# need to override this
	protected function _renderModeElement()
	{
		return "<input type='hidden' " .
					"name='mode' " .
					"value='" . $this->_mode . "' " .
				"/>";
	}

	protected function _renderPageElement()
	{
		return 	"<input " .
					"type='hidden' " .
					"name='page' " .
					"value='" . $this->_page . "' " .
				"/>";
	}

	# FIXME - list, insert, update, search and delete need to override
	public function render()
	{
		print $this->_renderFormHeader();

		foreach ($this->_fields as $field)
		{
			print $field->renderLabel();
			print $field->renderFormElement();
			print $field->renderError();
			print $this->_renderBlankLine();
		}

		print $this->_renderFormFooter();
	}

	public function addField($field)
	{
		$this->_fields[] = $field;
	}

	public function getName()
	{
		return $this->_page;
	}
}

?>
