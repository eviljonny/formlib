<?php

require_once("src/Formlib/Forms/Form.php");

class ListForm extends Form
{
	protected function _renderTableHeader()
	{
		$tableHeader =
			"<table>\n" .
			"<thead>\n" .
			"<tr>\n"
		;

		foreach ($this->_fields as $field)
		{
			$tableHeader .=
				"<th class='tabelLabel'>\n" .
				$field->renderTableHeader() . "\n" .
				"</th>\n"
			;
		}

		$tableHeader .=
			"</tr>\n" .
			"</thead>\n" .
			"<tbody>\n"
		;

		return $tableHeader;
	}

	protected function _renderTableFooter()
	{
		$tableFooter =
			"</tbody>\n" .
			"</table>\n"
		;

		return $tableFooter;
	}

	# FIXME - list, insert, update, search and delete need to override
	public function render()
	{
		print $this->_renderFormHeader();

		print $this->_renderTableHeader();

		foreach ($this->_fields as $field)
		{
			print $field->renderTableValue();
		}

		print $this->_renderTableFooter();

		print $this->_renderFormFooter();
	}
}

?>
