<?php

require_once("src/Formlib/FormlibException.php");
require_once("src/Formlib/Forms/Form.php");
require_once("src/Formlib/Forms/ListForm.php");
require_once("src/Formlib/QuickDB/QuickDB.php");
require_once("src/Formlib/QuickDB/DBInspector.php");
require_once("src/Formlib/QuickDB/LookupTableFactory.php");

class Formlib
{
	public static $MODES = array(
		"new" => 1,
		"edit" => 2,
		"insert" => 3,
		"update" => 4,
		"list" => 5,
		"search" => 6,
		"delete" => 7
	);
	protected static $FORM_TYPE_CLASS_MAP = array(
		"new" => "Form",
		"edit" => "Form",
		"insert" => "Form",
		"update" => "Form",
		"list" => "ListForm",
		"search" => "Form",
		"delete" => "Form",
	);
	protected $_basedir = null;
	protected $_form = null;
	protected $_db = null;
	protected $_dbInspector = null;
	protected $_lookupTableFactory = null;
	protected $_request = null;
	protected $_server = null;
	protected $_mode = null;

	public function __construct($request, $server, $basedir=false)
	{
		if (!$basedir)
		{
			$basedir = getcwd();
		}

		$this->_request = $request;
		$this->_server = $server;
		$this->_basedir = $basedir;
		$this->_loadConfig();
		$this->_openDatabase();
		$this->_dbInspector = new DBInspector($this->_db);
		$this->_lookupTableFactory = new LookupTableFactory($this->_db);
		$this->_mode = $this->_getMode();

		$this->_loadForm($this->_getFormName());
	}

	protected function _getFormName()
	{
		if (array_key_exists("page", $this->_request))
		{
			return $this->_request['page'];
		}
		else
		{
			throw new FormlibException("No page requested");
		}
	}

	protected function _openDatabase()
	{
		$this->_db = new QuickDB(FormlibConfig::$database);
	}

	protected function _getScriptName()
	{
		$script = array_pop(
			explode(
				"/",
				$this->_server["SCRIPT_NAME"]
			)
		);

		return $script;
	}

	protected function _loadForm($formName)
	{
		$class = Formlib::$FORM_TYPE_CLASS_MAP[$this->_mode];

		$this->_form = new $class(
			$this->_getScriptName(),
			$this->_basedir,
			$formName,
			$this->_mode,
			$this->_dbInspector,
			$this->_lookupTableFactory
		);
	}

	protected function _loadConfig($configFile=false)
	{
		if (!$configFile)
		{
			$configFile = "/config/formlib_conf.php";
		}
		require_once($this->_basedir . $configFile);
	}

	protected function _getMode()
	{
		$mode = null;
		if (array_key_exists("mode", $this->_request))
		{
			$mode = $this->_request['mode'];
		}
		elseif (array_key_exists("defaultMode", FormlibConfig::$options))
		{
			$mode = FormlibConfig::$options['mode'];
		}
		else
		{
			throw new FormlibException("No mode requested and no default mode set");
		}

		if (array_key_exists($mode, self::$MODES))
		{
			return $mode;
		}
		else
		{
			throw new FormlibException("Mode \"" . $mode . "\" not valid");
		}
	}

	public function renderForm()
	{
		$this->_form->render();
	}
}
