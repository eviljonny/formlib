var formList = ["zebraordernumber","salesorderitemid","supplierref","supplier_id","bearer_id","vlan_id","zenaggregationrouter_id","terminationequipment_id","terminationport","notes","deleted"];

function supplierChange() {
	var url = "index.php?";

	for (var index in formList) {
		element = document.getElementById(formList[index]);
		if (formList[index] == "deleted") {
			if (element != null && element.checked == true) {
				url = url + formList[index] + "=on";
			} else {
				url = url + formList[index] + "=off";
			}
		} else {
			if (element != null && element.value != "") {
				url = url + formList[index] + "=" + element.value + "&";
			}	
		}
	}

	window.location = url;
}

function showMe() {
	var list = "";
	alert(document.getElementById("searchForm"));
	for (var index in formList) {
		element = document.getElementById(formList[index]);
		if (element != null && element.value != "") {
			list = list + formList[index] + " : " + element.value + " ---- ";
		}	
	}

	alert(list);
}
